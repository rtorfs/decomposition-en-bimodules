# Décomposition bimodulaire

décompose des graphes bipartis selon la 
[décomposition bimodulaire](https://doi.org/10.1007/978-3-540-30559-0_10)

## Résultats

le dossier results contient

- ```time.csv``` qui compare le nombre d'arrête des graphes avec le temps en seconde qu'il a fallu pour calculer leurs décompositions bimodulaires
- ```moreno_crime_crime_decomposition.txt``` et ```opsahl-collaboration_decomposition.txt``` obtenus réspéctivement par les commandes
``` java Main decompose file graphs/moreno_crime_crime.graph``` et 
``` java Main decompose file graphs/opsahl-collaboration_decomposition.graph```
- ```compressionData.csv``` qui contient les données récolltées pour la compression des graphes bipartis utilisés

## Usage

Il faut une version de java ≥ 11

la plupart des commandes sont sous le format

```
java Main [action] [graphe]
```
où action peut être
- ```decompose``` pour afficher l'arbre de décomposition du graphe biparti
- ```compress``` pour écrire sur la sortie standard le graphe sous notre format compressé
- ```compression``` pour afficher le taux de compression de notre format sans changement de l'ordre des sommets et avec
- ```csv_data``` pour afficher les données du graphe dans l'odre
  - le dernier argument de la commande
  - le nombre de sommets
  - le nombre d'arrêtes
  - la taille en octets sous notre format sans changement d'ordre des sommets
  - la taille en octets sous notre format avec changement d'ordre des sommets
  - la taille en octets sous forme de liste d'arrêtes en binaire

et où graphe peut être
- ```test1``` ou ```test2``` des graphes biparti par default
- ```file [fichier de graphe]``` où le fichier de graphe est un fichier contenant une liste d'arrêtes sous le même format que ceux du [projet KONECT](http://konect.cc/)
- ```compressed_file [fichier de graphe compressé]``` où le fichier de graphe compressé est un fichier en ```.graph``` de ce dépot, un fichier généré par l'action ```compress```

sauf la commande des graphes orientés qui est
```
java Main directed_data [graphe dirigé sous forme de liste d'arcs]
```
et affiche le nombre de module fort non triviaux du graphe puis les données du graphe dans l'odre
  - le dernier argument de la commande
  - le nombre de sommets
  - le nombre d'arrêtes
  - la taille en octets sous notre format sans changement d'ordre des sommets
  - la taille en octets sous notre format avec changement d'ordre des sommets
  - la taille en octets sous forme de liste d'arrêtes en binaire
  
puis fait de même pour le graphe où les sources sont coloriées en noir et les puits sont coloriés en blanc.
