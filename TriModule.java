import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

import java.util.function.Supplier;
import java.util.function.IntFunction;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
public class TriModule{

    static class Data{

	static enum Type { KS , SERIE , PARALLEL , PRIME;
	
			   @Override
			   public String toString(){
			       switch(this){
			       case KS: return "K+S";
			       case SERIE: return "series";
			       case PARALLEL: return "parallel";
			       case PRIME: return "prime";
			       default: throw new IllegalStateException();
			       }
			   }
	}
	
	Type type;
	TriGraph quotient;

	static Data DATA_KS = new Data(Type.KS);
	static Data DATA_SERIE = new Data(Type.SERIE);
	static Data DATA_PARALLEL = new Data(Type.PARALLEL);

	Data(Type t){
	    type = t;
	    quotient = null;
	}

	Data(TriGraph g, List<IntSet> l){
	    this(computeQuotient(g,l));
	}

	Data(TriGraph q){
	    this(Type.PRIME);
	    quotient = q;
	}

	void forgetUselessQuotient(){
	    if( type != Type.PRIME )
		quotient = null;
	}
	
	@Override
	public String toString(){
	    return type.toString();
	}

	public boolean isKS(){
	    return type == Type.KS;
	}

	public boolean isPrime(){
	    return type == Type.PRIME;
	}

	private void setQuotient(TriGraph q){
	    quotient = q;
	}

	private static <T extends IntSet> TriGraph.Color colorFromList(List<T> t, int i, TriGraph g){
	    T res = t.get(i);
	    if( res.size() > 1 ||
		(res.size() == 1 && g.isBicolor(res.min())) )
		return TriGraph.Color.BICOLOR;
	    else
		return g.colorOf(res.min());
	}

	static Comparator<Tree<IntSet,Integer>> childListComp(TriGraph g) {
	    return Comparator.comparingInt(
					   x -> {
					       if( x.isSingleton() )
						   return colorOrder(g,x.getLeaf());
					       else
						   return 0;
					   });
	}

	/** g et une liste des modules maximaux */
	public static <T extends IntSet> TriGraph computeQuotient(TriGraph g, List<T> l){
	    IntStream withWhite =
		IntStream.range(0,l.size())
		.filter( i -> colorFromList(l,i,g) != TriGraph.Color.BLACK );
	    Supplier<IntStream> withBlack = () ->
		IntStream.range(0,l.size())
		.filter( i -> colorFromList(l,i,g) != TriGraph.Color.WHITE );

	    Stream<IntPaire> arcs = Tool.cartesianProduct(withWhite, withBlack)
		.filter(IntPaire::different)
		.filter( p -> g.isBlackVoisin( g.getAsWhite(l.get(p.fst)) , g.getAsBlack(l.get(p.snd)) ) )
		.map(IntPaire::rev);
	    
	    return TriGraph.Immutable.ofArcs(l.size(), arcs, i -> colorFromList(l,i,g) );
	}
    }

    /*static class TypedSet extends IntSet{
	Data.Type type;

	TypedSet(int x){
	    super(x);
	    type = Data.Type.KS;
	}

	TypedSet(int start, int end){
	    super(start,end);
	    type = Data.Type.KS;
	}

	TypedSet(Data.Type t, IntSet s){
	    super(s);
	    type = t;
	}

	TypedSet(Data.Type t, Set<Integer> s){
	    super(s);
	    type = t;
	}

	public static int compare(TypedSet d1, TypedSet d2){
	    int y = Integer.compare(d1.size(),d2.size());
	    if( y != 0 )
		return -y;
	    
	    int x = d1.modLexicoSort(d2);
	    if( x != 0 )
		return x;
	    
	    if( d1.type == Data.Type.KS )
		if( d2.type == Data.Type.KS )
		    return 0;
		else
		    return 1;
	    else
		if( d2.type == Data.Type.KS )
		    return -1;
		else
		    return 0;
	}
	}*/

    private static Optional<Tree<Data,Integer>> nonKSBiModuleComp(TriGraph q, boolean compl){
	Data.Type t = compl ? Data.Type.SERIE : Data.Type.PARALLEL;
	List<Set<Integer>> comp = StronglyConnectedComponents.compute(q,compl);
	if( comp.size() > 1 )
	    return Optional.of(Tree.withChilds(new Data(t),comp.stream()
					       .map(Set::stream)
					       .map( s ->
						     s.collect(Collectors.toCollection(ArrayList::new)))
					       .map( l -> Tree.withElements(new Data(Data.Type.KS),l) )
					       .collect(Collectors.toCollection(ArrayList::new))
					       ));
	else
	    return Optional.empty();
    }
			    
    private static Tree<Data,Integer> nonKSBiModuleDetermineType(TriGraph q){
	return nonKSBiModuleComp(q, false)
	    .or(() ->nonKSBiModuleComp(q, true))
	    .orElseGet(() -> Tree.withElements(new Data(q),
					       q.vertices()
					       .boxed()
					       .collect(Collectors.toList()) )
		       );
    }

    /*private static boolean isKS(TriGraph q){
	int numWhite = q.whiteSize() + q.bicolorSize();
	int numBlack = q.blackSize() + q.bicolorSize();
	return q.vertices().anyMatch​( v -> {
		switch(q.colorOf(v)){
		case WHITE:
		    return q.blackDegreeOf(v) == 0;
		case BLACK:
		    return q.whiteDegreeOf(v) == numWhite;
		case BICOLOR:
		    return q.blackDegreeOf(v) == 0 && q.whiteDegreeOf(v) == numWhite - 1;
		}
		return false;
	    });
	    }*/

    /** renvoi les bimodules canonique n'étant pas des K+S, trié par taille puis lexicographiquement */
    private static Tree<Data,Integer> nonKSBiModule(TriGraph g){
	boolean[] foundTop = {false};
	Tree<IntSet,Integer> implClass = CoComparability.implicationClassTree(g,foundTop);
	for(Tree<IntSet,Integer> t : implClass)
	    if( !t.isSingleton() )
		t.sort( Data.childListComp(g) );

	Main.log.messageAge("sorted childs");

	Tree<Data,Integer> res = implClass.map(x->x, c -> {
		Data dt = new Data(g,c.childsStream().
				   map( t -> t.computeLeafOrNode(IntSet::of, (n,ouo) -> n ) )
				   .collect(Collectors.toCollection(ArrayList::new)));
		
		return nonKSBiModuleDetermineType(dt.quotient);
	    }, (t,c) -> {
		t.extendLeaf(c::get);
		Data.Type ty = t.getNode().type;
		if( ty == Data.Type.SERIE || ty == Data.Type.PARALLEL )
		    t.absorbChilds( tt -> tt.getNode().type == ty );
		t.getNode().forgetUselessQuotient();
	    });

	if( !foundTop[0] && g.size() > 1 ){
	    res.getNode().type = Data.Type.KS;
	    res.getNode().forgetUselessQuotient();
	}
	return res;
    }

    private static boolean atLeast3(TriGraph g, int a, int b){
	return g.isBicolor(a) || g.isBicolor(b);
    }
    private static boolean atLeast3(TriGraph d, IntSet s){
	return s.stream().mapToObj(d::isBicolor).mapToInt( x -> x ? 2 : 1).sum() >= 3;
    }

    private static Set<IntSet> allSmallKS(TriGraph g){
	int[][] onlyDifference = g.vertices() // oD[a][b] = c ssi c est le seul sommet qui dist a de b, si aucun dist -1, sinon -2
	    .mapToObj( b ->
		       IntStream.range(0,b)
		       .map( a -> {
			       if( !g.isComparable(a,b) )
				   return -1;
			       
			       IntSet differents =
				   g.whiteVoisinsOf(a).xor(g.whiteVoisinsOf(b))
				   .union( g.blackVoisinsOf(a).xor(g.blackVoisinsOf(b)) )
				   .filter( x -> x != a && x != b );

			       if( differents.size() < 1 )
				   return -1;
			       else if( differents.size() > 1 )
				   return -2;
			       else
				   return differents.min();
			   })
		       .toArray()
		       )
	    .toArray(int[][]::new);
	
	return g.vertices().boxed()
	    .flatMap( i -> IntStream.range(0,i).mapToObj( j -> new IntPaire(j,i) ) )
	    .map( p -> {
		    int a = p.fst; int b = p.snd;
		    int diff = onlyDifference[b][a];

		    if( diff == -2 )
			return null;
		    else if( diff == -1 )
			return atLeast3(g,a,b) ? IntSet.of(a,b) : null;

		    int bd = diff > b ? onlyDifference[diff][b] : onlyDifference[b][diff];
		    int ad = diff > a ? onlyDifference[diff][a] : onlyDifference[a][diff];
		    if( (bd == -1 || bd == a)
			&& (ad == -1 || ad == b) )
			return IntSet.of(a,b,diff);
		    else
			return null;
		})
	    .filter( p -> p != null )
	    .collect( Collectors.toUnmodifiableSet() );
    }

    static int colorOrder(TriGraph g, int x){
	switch(g.colorOf(x)){
	case BLACK: return 1;
	case WHITE: return -1;
	default: return 0;
	}
    }

    private static Set<IntSet> smallKSWhithoutTriangleConflict(Set<IntSet> s, TriGraph g){
	Set<Integer> maybe = new HashSet<>();
	Set<Integer> triInterdit = new HashSet<>();
	
	s.stream()
	    .filter( is -> is.size() == 3 )
	    .map( is ->
		  is.stream().boxed()
		  .sorted( Comparator.comparingInt( x -> colorOrder(g,x) ) )
		  .mapToInt(i->i).toArray() )
	    .filter( is -> g.isWhite(is[0]) )
	    .filter( is -> g.isBlack(is[2]) )
	    .forEachOrdered( is -> {
		    int dist = -1;
		    switch( g.colorOf(is[1]) ){
		    case WHITE: dist = is[2]; break;
		    case BLACK: dist = is[0]; break;
		    default: return;
		    }

		    if( maybe.contains(dist) )
			triInterdit.add(dist);
		    else
			maybe.add(dist);
		});
	
	return s.stream()
	    .flatMap( mod -> {
		    IntSet dos = mod.filter( x -> !triInterdit.contains(x) );
		    if( atLeast3(g,dos) )
			return Stream.of( dos );
		    else
			return Stream.empty();
		}).collect( Collectors.toUnmodifiableSet() );
    }

    private static Set<IntSet> modulesKS(TriGraph g){
	Set<IntSet> sansConflitTriangle = smallKSWhithoutTriangleConflict(allSmallKS(g),g);
	return SetOverlapClass.resolveBicolor(sansConflitTriangle, g);
    }

    private static int linearSorting(TriGraph g, int s1, int s2){
	
	if( g.isWhite(s1) && g.isWhite(s2) )
	    return Integer.compare(g.blackDegreeOf(s1),g.blackDegreeOf(s2));
	else if( g.isWhite(s1) && g.isBlack(s2) )
	    return g.isBlackVoisin(s1,s2) ? 1 : -1;
	else if( g.isBlack(s1) && g.isWhite(s2) )
	    return g.isWhiteVoisin(s1,s2) ? -1 : 1;
	else
	    return -Integer.compare(g.whiteDegreeOf(s1),g.whiteDegreeOf(s2));
    }
    
    /** partitionne en bimodules maximaux */
    private static List<IntSet> ksPrimeChild(TriGraph g){
	Set<IntSet> mods = modulesKS(g);
	
	Set<Integer> seen = mods.stream()
	    .flatMapToInt( IntSet::stream )
	    .boxed().collect( Collectors.toUnmodifiableSet() );
	
	return Stream.concat( mods.stream() , 
			      g.vertices()
			      .filter( x -> !seen.contains(x) )
			      .boxed()
			      .map( IntSet::of ) )
	    .sorted(Comparator.comparingInt(
					    x -> {
						if( x.size() == 1 )
						    return colorOrder(g, x.min());
						else
						    return 0;
					    }))
	    .collect(Collectors.toCollection(ArrayList::new));
    }
    
    public static Tree<Data,Integer> decomposition(TriGraph g){
	if( g instanceof TriGraph.ModuleTree )
	    return ((TriGraph.ModuleTree) g).getTrimodularDecomposition();
	
        Tree<Data,Integer> partialTree = nonKSBiModule(g);
	Main.log.messageAge("partial tree structure done");

	Tree<Data,Integer> finalTree = partialTree.map(x->x, ttt -> {
		Data dt = ttt.getNode();
		
		if( dt.isPrime() ){
		    List<IntSet> part = ksPrimeChild(dt.quotient);
		    List<Tree<Data,Integer>> newChilds = part
			.stream().map( IntSet::stream )
			//.map( s -> s.mapToObj(childs::get) )
			.map(IntStream::boxed)
			.map( s -> s.collect(Collectors.toCollection(ArrayList::new)) )
			.map( l -> Tree.withElements(new Data(Data.Type.KS),l) )
			//.sorted( )
			.collect(Collectors.toCollection(ArrayList::new));

		    return Tree.withChilds(new Data(dt.quotient,part),newChilds);
		}
		else
		    return Tree.withElements(dt,IntStream
					     .range(0,ttt.degree())
					     .boxed()
					     .collect( Collectors.toCollection(ArrayList::new) ) );
	    }, (t,c) -> {
		t.extendLeaf(c::get);
	    });
	Main.log.messageAge("K+S childs of primes done");

	for(Tree<Data,Integer> t : finalTree)
	    if( t.isNodeAnd(Data::isKS) )
		t.sort( (x,y) -> linearSorting(g,x.getLeaf(),y.getLeaf()) );
	
	return finalTree;
    }

    private static enum DataType{
	PRIME, SERIE, PARALLEL, KS, WHITE, BLACK, BICOLOR;

	byte toByte(){
	    switch(this){
	    case PRIME: return 0;
	    case SERIE: return 1;
	    case PARALLEL: return 2;
	    case KS: return 3;
	    case WHITE: return 4;
	    case BLACK: return 5;
	    case BICOLOR: return 6;
	    }
	    throw new IllegalStateException();
	}

	boolean isPrime(){
	    return this == PRIME;
	}

	static DataType ofByte(byte b){
	    switch(b){
	    case 0: return PRIME;
	    case 1: return SERIE;
	    case 2: return PARALLEL;
	    case 3: return KS;
	    case 4: return WHITE;
	    case 5: return BLACK;
	    case 6: return BICOLOR;
	    }
	    throw new IllegalStateException();
	}

	TriGraph.Color toColor(){
	    switch(this){
	    case WHITE: return TriGraph.Color.WHITE;
	    case BLACK: return TriGraph.Color.BLACK;
	    case BICOLOR: return TriGraph.Color.BICOLOR;
	    }
	    throw new IllegalStateException();
	}

	static DataType ofColor(TriGraph.Color c){
	    switch(c){
	    case WHITE: return WHITE;
	    case BLACK: return BLACK;
	    case BICOLOR: return BICOLOR;
	    }
	    throw new IllegalStateException();
	}

	Data.Type toType(){
	    switch(this){
	    case PRIME: return Data.Type.PRIME;
	    case KS: return Data.Type.KS;
	    case SERIE: return Data.Type.SERIE;
	    case PARALLEL: return Data.Type.PARALLEL;
	    }
	    throw new IllegalStateException();
	}

	static DataType ofType(Data.Type c){
	    switch(c){
	    case PRIME: return PRIME;
	    case KS: return KS;
	    case SERIE: return SERIE;
	    case PARALLEL: return PARALLEL;
	    }
	    throw new IllegalStateException();
	}
    }

    public static void writeTo(TriGraph g, boolean saveName, DataOutput out) throws IOException{
	writeTo(decomposition(g),g::colorOf,saveName,out);
    }

    public static void writeTo(Tree<Data,Integer> t, IntFunction<TriGraph.Color> color, boolean saveName, DataOutput out) throws IOException{
	out.writeBoolean( saveName );
	writeToAux(color,t,saveName,out);
    }

    private static void writeToAux(IntFunction<TriGraph.Color> color, Tree<Data,Integer> t, boolean withName, DataOutput out) throws IOException{
	if( t.isSingleton() ){
	    int v = t.getLeaf();

	    out.writeByte( DataType.ofColor( color.apply(v) ).toByte() );
	    if( withName )
		Tool.writePositiveInt(out, v);
	}else{
	    Data dt = t.getNode();

	    out.writeByte( DataType.ofType(dt.type).toByte() );
	    Tool.writePositiveInt(out, t.degree() );

	    if( dt.isPrime() ){

		int bs = dt.quotient.blackSize();
		int ws = dt.quotient.whiteSize();

		Tool.writePositiveInt(out, ws);
		Tool.writePositiveInt(out, bs);
		
		if( withName )
		    for(int i = 0; i < ws; i++)
			Tool.writePositiveInt(out, t.child(i).getLeaf() );

		for(int i = ws; i < t.degree() - bs; i++)
		    writeToAux(color,t.child(i),withName,out);

		if( withName )
		    for(int i = t.degree() - bs; i < t.degree(); i++)
			Tool.writePositiveInt(out, t.child(i).getLeaf() );
		
		dt.quotient.writeOn(out);
	    }else{
		for(Tree<Data,Integer> c : t.childs())
		    writeToAux(color,c,withName,out);
	    }
	    
	}
    }

    public static class IntegerColor{
	final int num;
	final TriGraph.Color color;

	IntegerColor(int x, TriGraph.Color c){
	    num = x;
	    color = c;
	}

	public String toString(){
	    return ""+color+" "+num;
	}
    }

    private static interface Naming{
	int next() throws IOException;
    }
    private static class CounterNaming implements Naming{

	int num;

	CounterNaming(){
	    num = 0;
	}

	public int next(){
	    int tmp = num;
	    num++;
	    return tmp;
	}
    }
    private static class ReaderNaming implements Naming{

	DataInput dt;

	ReaderNaming(DataInput f){
	    dt = f;
	}

	public int next() throws IOException{
	    return Tool.readPositiveInt(dt);
	}
    }
    

    public static Tree<Data,IntegerColor> readFrom(DataInput in) throws IOException{
	Naming newNames = in.readBoolean() ? new ReaderNaming(in) : new CounterNaming();
	return readFromAux(newNames,in);
    }

    private static Tree<Data,IntegerColor> readFromAux(Naming newNames, DataInput in) throws IOException{
	DataType dt = DataType.ofByte( in.readByte() );
	switch( dt ){
	case BICOLOR:
	case WHITE:
	case BLACK:
	    return new Tree<>( new IntegerColor( newNames.next(), dt.toColor() ) );
	}

	int deg = Tool.readPositiveInt(in);
	List<Tree<Data,IntegerColor>> childs = new ArrayList<>(deg);

	if( dt.isPrime() ){
	    int numWhite = Tool.readPositiveInt(in);
	    int numBlack = Tool.readPositiveInt(in);
	    
	    for(int i = 0; i < numWhite; i++)
		childs.add(new Tree<>( new IntegerColor(newNames.next(),TriGraph.Color.WHITE) ));
	    for(int i = 0; i < deg - (numWhite + numBlack); i++)
		childs.add( readFromAux(newNames,in) );
	    for(int i = 0; i < numBlack; i++)
		childs.add(new Tree<>( new IntegerColor(newNames.next(),TriGraph.Color.BLACK) ));
	    
	    IntFunction<TriGraph.Color> qColor = i -> {
		Tree<Data,IntegerColor> tmp = childs.get(i);
		if( tmp.isSingleton() )
		    return tmp.getLeaf().color;
		else
		    return TriGraph.Color.BICOLOR;
		
	    };
	    TriGraph q = TriGraph.readFrom( (color,arcs) -> TriGraph.Immutable.ofArcs(childs.size(),arcs,color), qColor, in);
	    return Tree.withChilds(new Data(q), childs);
	}
	else{
	    for(int i = 0; i < deg; i++)
		childs.add( readFromAux(newNames,in) );
	    return Tree.withChilds(new Data(dt.toType()), childs);
	}
    }
}
