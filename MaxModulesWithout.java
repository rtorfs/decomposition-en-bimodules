import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import java.util.Stack;

import java.util.Set;
import java.util.HashSet;
public class MaxModulesWithout{

    static class UnprocessedSet{
	Set<Integer> inside;
	Set<Integer> z;

	public String toString(){
	    return "{ S="+inside+" "+"Z(S)="+z+" }";
	}
	    
	UnprocessedSet(int size, int v){
	    inside = new HashSet<>();
	    z = new HashSet<>();
	    for(int i = 0; i < size; i++)
		if( i == v )
		    z.add(i);
		else
		    inside.add(i);
	}
	
	UnprocessedSet(UnprocessedSet s, Set<Integer> w){
	    inside = w;
	    
	    z = new HashSet<>();
	    z.addAll( s.inside );
	    z.removeAll(w);
	    z.addAll(s.z);
	}
	    
	private List<Set<Integer>> split(IntSet v1, IntSet v2){
	    List<Set<Integer>> res = new ArrayList<Set<Integer>>(4);
	    for(int i = 0; i < 4; i++)
		res.add(new HashSet<Integer>());
		
	    for(int x : inside){
		int a = v1.contains(x) ? 1 : 0;
		int b = v2.contains(x) ? 1 : 0;
		
		res.get(a + b*2).add(x);
	    }
	    return res;
	}
	    
	List<UnprocessedSet> divide(IntSet v1, IntSet v2){
	    List<Set<Integer>> ll = split(v1,v2);

	    List<UnprocessedSet> res = new ArrayList<>();
	    for(Set<Integer> hs : ll){
		if( hs.isEmpty() )
		    continue;
		    
		res.add(new UnprocessedSet(this,hs));
	    }
	    return res;
	}
    }
	
    /**
     * renvoi les modules les plus grands de g ne comprenend pas v
     * prend en entré g et son graphe "graphe inversé" ( (a,b) -> (b,a) ) rg
    */
    public static List<Set<Integer>> compute(Graph g, Graph rg, int v){
	List<Set<Integer>> res = new ArrayList<>();
	Stack<UnprocessedSet> calc = new Stack<>();
	calc.add(new UnprocessedSet(g.size(), v));

	while( !calc.isEmpty() ){
	    UnprocessedSet x = calc.pop();

	    int pivot = x.z.iterator().next();
	    x.z.remove(pivot);

	    List<UnprocessedSet> l = x.divide( g.voisins(pivot) , rg.voisins(pivot) );

	    for(UnprocessedSet us : l)
		if( us.z.isEmpty() )
		    res.add(us.inside);
		else
		    calc.push(us);
	}
	
	return res;
    }
}
