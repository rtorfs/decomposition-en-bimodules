import java.util.Objects;
public class Paire<U>{
    final U fst;
    final U snd;

    Paire(U x, U y){
	fst = x;
	snd = y;
    }

    @Override
    public String toString(){
	return "("+fst+";"+snd+")";
    }

    @Override
    public int hashCode(){
     	return Objects.hashCode(fst) + Objects.hashCode(snd) * 7919;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean equals(Object o){
	if( o != null && o.getClass() == this.getClass() ){
	    Paire<U> p = (Paire<U>) o;
	    return Objects.equals(p.fst,this.fst) && Objects.equals(p.snd,this.snd);
	}else{
	    return false;
	}
    }

    /** (x,y) -> (y,x) */
    public Paire<U> rev(){
	return new Paire<>(snd,fst);
    }
}
