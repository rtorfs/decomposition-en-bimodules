import java.util.Comparator;

import java.util.List;

import java.util.Set;
import java.util.HashSet;

import java.util.function.IntFunction;
public class Module{

    static class Data{
	enum Type{ SERIE, PARALLEL, LINEAR, PRIME }

	Type type;
	Graph quotient;
	
	@Override
	public String toString(){
	    switch(type){
	    case SERIE: return "serie";
	    case PARALLEL: return "parallel";
	    case PRIME: return "prime";
	    case LINEAR: return "linear";
	    default : throw new IllegalStateException();
	    }
	}

	Data(Type t){
	    type = t;
	    quotient = null;
	}

	Data(Graph g){
	    type = Type.PRIME;
	    quotient = g;
	}

	boolean isType(Type t){
	    return type == t;
	}

	Graph getQuotient(){
	    if( type != Type.PRIME )
		throw new IllegalStateException();
	    else
		return quotient;
	}

	void setQuotient(Graph g){
	    if( type != Type.PRIME )
		throw new IllegalStateException();
	    else
		quotient = g;
	}

	Type getType(){
	    return type;
	}

	static boolean bothSerie(Data x, Data y){
	    return Type.SERIE == x.type && Type.SERIE == y.type;
	}

	static boolean bothLinear(Data x, Data y){
	    return Type.LINEAR == x.type && Type.LINEAR == y.type;
	}

	static boolean bothParallel(Data x, Data y){
	    return Type.PARALLEL == x.type && Type.PARALLEL == y.type;
	}
	
    }

    /**
     * calcul de la decomposition modulaire d'un graphe
     * source:
     *
     * An O(n2) Divide-and-Conquer Algorithm for the Prime Tree Decomposition of Two-Structures and Modular Decomposition of Graphs
     * A. Ehrenfeucht and H.N. Gabow and R.M. Mcconnell and S.J. Sullivan
     */

    private static Data moduleType(Graph g, Set<Set<Integer>> F, int removedSinkNum){
	if( removedSinkNum == 1 && F.size() > 1 ){
	    return new Data(Data.Type.PRIME);
	}else{
	    int x = F.iterator().next().iterator().next();
	    
	    if( g.isVoisin(x,0) && g.isVoisin(0,x) )
		return new Data(Data.Type.SERIE);
	    else if( !g.isVoisin(x,0) && !g.isVoisin(0,x) )
		return new Data(Data.Type.PARALLEL);
	    else
		return new Data(Data.Type.LINEAR);
	}
    }

    public static Tree<Data,Integer> decomposition(Graph g){
	return decomposition(g,g.reverse());
    }
    
    public static Tree<Data,Integer> decomposition(Graph g, Graph rg){
	Tree<Data,Integer> res = decompositionAux(g, rg);

	for(Tree<Data,Integer> t : res)
	    if( !t.isSingleton() )
		switch( t.getNode().getType() ){
		case LINEAR:
		    t.sort( Comparator.comparingInt( (x) -> -g.degree(x.getLeaf()) ) );
		    break;
		case PRIME:
		    List<Integer> l = t.childsMap( Tree::getLeaf );
		    t.getNode().setQuotient(new Graph(l.size(), (x,y) -> g.isVoisin(l.get(x),l.get(y)) ));
		    break;
		}
	
	return res;
    }

    /**
     * rend l'arbre de décomposition modulaire de g
     */
    private static Tree<Data,Integer> decompositionAux(Graph g, Graph rg){
	if(g.size() == 1)
	    return new Tree<>(0);
	if(g.size() == 0)
	    throw new IllegalArgumentException();

	List<Set<Integer>> partition = MaxModulesWithout.compute(g,rg,0);
	InterfaceGraph<Set<Integer>,Graph> g1 = G(g,0,partition);
	
	InterfaceGraph<Set<Set<Integer>>,Graph> g2 = g1.applySetGraph( Graph::componentGraph );
	
	Tree<Data,Integer> root = new Tree<>();
	Tree<Data,Integer> u = root;
	
	while( g2.size() > 0 ){
	    if( !u.isEmpty() ){
		Tree<Data,Integer> future = new Tree<>();
		u.addChild(future);
		u = future;
	    }
	    
	    InterfaceGraph<Set<Set<Integer>>,Graph> noSink = g2.apply( (h) -> h.filter( (x) -> h.degree(x) > 0 ) );
	    
	    Set<Set<Integer>> F = new HashSet<>();
	    for(Set<Set<Integer>> s : g2.computeSet(Graph::sinks))
		F.addAll(s);
	    
	    u.setNode( moduleType(g,F, g2.size() - noSink.size() ) );
	    
	    for(Set<Integer> X : F){
		InterfaceGraph<Integer,Graph> gx = g.filter( X::contains );

		Tree<Data,Integer> tx = gx.computeTree( (h) -> decompositionAux(h,h.reverse()) );

		if( !tx.isSingleton() && (Tree.compareNodes(u,tx, Data::bothSerie )  ||
					  Tree.compareNodes(u,tx, Data::bothParallel ) ||
					  Tree.compareNodes(u,tx, Data::bothLinear ) ))
		    u.fusion(tx);
		else
		    u.addChild(tx);
	    }
	    g2 = noSink;
	}
	
	u.addChild(new Tree<>(0));
	return root;
    }

    /**
       Graphe g\partition sans v avec un arc de x vers y ssi x dist y de v dans g
       partition est une partition de g sans v
     */
    private static InterfaceGraph<Set<Integer>,Graph> G(Graph g, int v, List<Set<Integer>> partition){
	Graph bigG = new Graph(partition.size(), (x,y) -> {
		int rx = partition.get(x).iterator().next();
		int ry = partition.get(y).iterator().next();
		
		return g.distingue(rx,ry,v);
	});
	return new InterfaceGraph<>(bigG,partition);
    }
    
}
