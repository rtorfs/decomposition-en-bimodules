import java.util.Collection;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

import java.util.function.IntPredicate;
import java.util.function.IntFunction;
import java.util.function.BiFunction;
import java.util.function.IntUnaryOperator;

import java.util.Arrays;
public class IntSet implements Iterable<Integer>, Comparable<IntSet>{

    private final int[] data;
    public static final IntSet EMPTY = new IntSet();

    /** 
     * l'ensemble des éléments de c
     */
    public IntSet(IntStream c){
	data = c.sorted().distinct().toArray();
    }

    public IntSet(Collection<Integer> c){
	this( c.stream().mapToInt( i -> i ) );
    }

    /** [start;end[ */
    public IntSet(int start, int end){
	this(IntStream.range(start,end));
    }

    /** 
     * l'ensemble des éléments de c
     * suppose que c est sans doublons et de taille size
     */
    /*public IntSet(int size, Iterable<Integer> c){
	
      }*/

    public IntSet(IntSet is){
	this.data = is.data;
    }

    /** 
     * l'ensemble des éléments de a
     */
    public IntSet(int[] a){
	this(Arrays.stream(a));
    }

    /** 
     * l'ensemble des éléments de a
     * suppose que a est sans doublons
     */
    public IntSet(Integer[] a){
	data = new int[a.length];
	int prev = Integer.MIN_VALUE;
	boolean sorted = true;
	for(int i = 0; i < a.length; i++){
	    data[i] = a[i];
	    if( data[i] < prev )
		sorted = false;
	    prev = data[i];
	}
	if( !sorted )
	    Arrays.sort(data);
    }

    /** le singleton {x} */
    public IntSet(int x){
	data = new int[]{ x };
    }

    private IntSet(int size, boolean dummy){
	data = new int[size];
    };

    /** l'ensemble vide */
    public IntSet(){
	data = new int[0];
    }

    public int size(){
	return data.length;
    }

    /** renvoi si x appartient à this */
    public boolean contains(int x){
	return Arrays.binarySearch​(data,x) >= 0;
    }

    public boolean subSetOf(IntSet s){
	Inclusion res = this.inclusionType(s);
	return res == Inclusion.SUBSET || res == Inclusion.EQUAL;
    }

    public Iterator<Integer> iterator(){
	return new Iterator<>(){
	    int i = 0;

	    public boolean hasNext(){
		return i < data.length;
	    }

	    public Integer next(){
		i++;
		return data[i-1];
	    }
	};
    }

    public <T> T operation(IntSet s, T seed,
			   BiFunction<Integer,T,T> aMinB,
			   BiFunction<Integer,T,T> bMinA,
			   BiFunction<Integer,T,T> both){
	T res = seed;
	int thisPos = 0;
	int sPos = 0;
	
	while( thisPos < this.size() && sPos < s.size() ){
	    final int x = this.data[thisPos];
	    final int sx = s.data[sPos];
	    if( x < sx ){
		res = aMinB.apply(x,res);
		thisPos++;
	    }else if( x == sx ){
		res = both.apply(x,res);
		thisPos++;
		sPos++;
	    }else{
		res = bMinA.apply(sx,res);
		sPos++;
	    }
	}
	for(int i = thisPos; i < this.size(); i++)
	    res = aMinB.apply(this.data[i],res);
	for(int i = sPos; i < s.size(); i++)
	    res = bMinA.apply(s.data[i],res);
	return res;
    }

    public IntSet xor(IntSet s){
	ArrayList<Integer> res = new ArrayList<>();
	
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> adding = (x,l) -> {
	    l.add(x);
	    return l;
	};
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> nothing = (x,l) -> l;
	
	return new IntSet( operation(s, res, adding, adding, nothing) );
    }

    public IntSet inter(IntSet s){
	ArrayList<Integer> res = new ArrayList<>();
	
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> adding = (x,l) -> {
	    l.add(x);
	    return l;
	};
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> nothing = (x,l) -> l;
	
	return new IntSet( operation(s, res, nothing, nothing, adding) );
    }

    public IntSet setMinus(IntSet s){
	ArrayList<Integer> res = new ArrayList<>();
	
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> adding = (x,l) -> {
	    l.add(x);
	    return l;
	};
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> nothing = (x,l) -> l;
	
	return new IntSet( operation(s, res, adding, nothing, nothing) );
    }

    public IntSet union(IntSet s){
	ArrayList<Integer> res = new ArrayList<>();
	
	BiFunction<Integer,ArrayList<Integer>,ArrayList<Integer>> adding = (x,l) -> {
	    l.add(x);
	    return l;
	};
	
	return new IntSet( operation(s, res, adding, adding, adding) );
    }

    public IntSet filter(IntPredicate p){
	ArrayList<Integer> l = new ArrayList<>();
	for(int i = 0; i < data.length; i++)
	    if( p.test( data[i] ) )
		l.add(data[i]);
	return new IntSet(l);
    }

    /** suppose que f est croissante strictement */
    public IntSet map(IntUnaryOperator f){
	IntSet res = new IntSet(this.data.length,true);
	for(int i = 0; i < data.length; i++)
	    res.data[i] = f.applyAsInt( this.data[i] );
	return res;
    }

    public IntSet filterMap(IntPredicate p, IntUnaryOperator f){
	return new IntSet( stream().filter(p).map(f).toArray() );
    }
    
    /** compare lexicographiquement a et b */
    public int compareTo(IntSet s){
	return Arrays.compare(this.data,s.data);
    }

    /** lexico sort mais si this subset s ou s subset this renvoi 0 */
    public int modLexicoSort(IntSet s){
	int thisPos = 0;
	int sPos = 0;

	while( thisPos < this.size() && sPos < s.size() ){
	    int x = this.data[thisPos];
	    int sx = s.data[sPos];

	    if( x != sx )
		return x > sx ? 1 : -1;
	    
	    thisPos++;
	    sPos++;
	}

	return 0;
    }

    public boolean forAll(IntPredicate p){
	for(int x : data)
	    if( !p.test(x) )
		return false;
	return true;
    }

    public boolean exists(IntPredicate p){
	for(int x : data)
	    if( p.test(x) )
		return true;
	return false;
    }

    public static IntSet of(Integer... a){
	return new IntSet(a);
    }

    @Override
    public boolean equals(Object o){
	if( o instanceof IntSet )
	    return Arrays.equals(data,((IntSet) o).data);
	else
	    return false;
    }

    @Override
    public int hashCode(){
	return Arrays.hashCode(data);
    }

    static enum Inclusion{ EQUAL, SUBSET, SUPERSET, OVERLAP , DISJOINT }
    private class InsideBool{
	boolean insideThis = false;
	boolean insideS = false;
	boolean insideBoth = false;

	public InsideBool seeThis(){
	    insideThis = true;
	    return this;
	}

	public InsideBool seeS(){
	    insideS = true;
	    return this;
	}

	public InsideBool seeBoth(){
	    insideBoth = true;
	    return this;
	}

	public Inclusion getType(){
	    if( insideBoth ){
		if( !insideS && !insideThis )
		    return Inclusion.EQUAL;
		if( insideS && !insideThis )
		    return Inclusion.SUBSET;
		if( !insideS && insideThis )
		    return Inclusion.SUPERSET;
		if( insideS && insideThis )
		    return Inclusion.OVERLAP;
	    }else{
		return Inclusion.DISJOINT;
	    }
	    return Inclusion.DISJOINT;
	}
    }
    public Inclusion inclusionType(IntSet s){
	InsideBool inside = new InsideBool();

	return operation(s,inside, (x,i) -> i.seeThis(), (x,i) -> i.seeS(), (x,i) -> i.seeBoth() ).getType();
    }

    public int min(){
	return data[0];
    }

    public int max(){
	return data[data.length-1];
    }

    public int get(int i){
	return data[i];
    }

    public IntStream stream(){
	return Arrays.stream(data);
    }

    @Override
    public String toString(){
	return toString(Integer::toString);
    }

    public String toString(IntFunction<String> printer){
	return stream()
	    .mapToObj(printer)
	    .collect( Collectors.joining(", ","[","]") );
    }
}
