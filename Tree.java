import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Stack;
import java.util.NoSuchElementException;
import java.util.Arrays;

import java.util.Set;
import java.util.HashSet;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;

import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

import java.util.function.Predicate;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.util.function.BiConsumer;
import java.util.function.IntUnaryOperator;
public class Tree<U,T> implements Iterable<Tree<U,T>> {
    
    private T leaf;
    private U node;
    private List<Tree<U,T>> childs;

    /** un arbre n'ayant qu'un sommet */
    Tree(T x){
	node = null;
	leaf = x;
	childs = null;
    }

    Tree(){
	node = null;
	leaf = null;
	childs = new ArrayList<>();
    }

    public int size(){
	if( isSingleton() )
	    return 0;
	else
	    return 1 + childs.stream().mapToInt(Tree::size).sum();
    }

    /** un noeud de type nt ayant pour enfants les élément de l dans cet ordre */
    private Tree(U nt, List<Tree<U,T>> l){
	node = nt;
	leaf = null;
	childs = l == null ? new ArrayList<>() : l;
    }
    public static <T,U> Tree<U,T> withChilds(U nt, List<Tree<U,T>> l){
	return new Tree<>(nt,l);
    }
    public static <T,U> Tree<U,T> withChilds(U nt){
	return withChilds(nt, (List<Tree<U,T>>) null);
    }
    @SuppressWarnings("unchecked")
    public static <T,U> Tree<U,T> withChilds(U nt, Tree... l){
	ArrayList<Tree<U,T>> ll = new ArrayList<>();
	for(Tree x : l)
	    ll.add(x);
	return withChilds(nt,ll);
    }
    
    /** un noeud de type nt avec comme enfant les singletons des élément de l dans cet ordre */
    public static <T,U> Tree<U,T> withElements(U nt, List<T> l){
	if( l.size() > 1 ){
	    List<Tree<U,T>> c = new ArrayList<>();
	    for(T x : l)
		c.add(new Tree<>(x));
	    
	    return withChilds(nt,c);
	}else{
	    return new Tree<>(l.get(0));
	}
    }

    protected Tree(Tree<U,T> t){
	node = t.node;
	leaf = t.leaf;
	childs = t.childs;
    }

    /** renvois is this est un singleton */
    public boolean isSingleton(){ return childs == null; }
    
    public <V> V computeLeafOrNode(Function<T,V> leafCase, BiFunction<U,Stream<Tree<U,T>>,V> nodeCase){
	if( isSingleton() )
	    return leafCase.apply( leaf );
	else
	    return nodeCase.apply( node , childs.stream() );
    }


    /*public <V,W> Tree<V,W> map(Function<T,V> leafMap, BiFunction<U,Stream<Tree<T,U>>,W> nodeMap){
	if( singleton() )
	    return new Tree<>( leafMap.apply(leaf) );
      	else
	    return new Tree<>( nodeMap.apply(node,childs.stream()) , childsMap( t -> t.map(leafMap,nodeMap) ) );
	    }*/

    /** nodeMap ne  modifie pas la filiation des noeud fils */
    public <V,W> Tree<W,V> map(Function<T,V> leafMap,
			       Function<Tree<U,T>,Tree<W,V>> nodeMap,
			       BiConsumer<Tree<W,V>,List<Tree<W,V>>> concat){
	if( isSingleton() )
	    return new Tree<>(leafMap.apply(leaf));
	
	ArrayList<Tree<U,T>> res = new ArrayList<>();
	ArrayList<List<Integer>> childsPos = new ArrayList<>();
	
	Stack<Tree<U,T>> wait = new Stack<>();
	Stack<Integer> father = new Stack<>();
	wait.push(this);
	father.push(-1);

	while( !wait.isEmpty() ){
	    Tree<U,T> cur = wait.pop();
	    int curFatherPos = father.pop();
	    int curPos = res.size();

	    res.add(cur);
	    childsPos.add(new ArrayList<>());
	    
	    if( curFatherPos >= 0 )
		childsPos.get(curFatherPos).add(curPos);

	    if( !cur.isSingleton() )
		for(int i = cur.childs.size() - 1; i >= 0; i--){
		    wait.push(cur.childs.get(i));
		    father.push(curPos);
		}
		
	}

	ArrayList<Tree<W,V>> mapped = res.parallelStream()
	    .map( t -> {
		    if( t.isSingleton() )
			return new Tree<W,V>(leafMap.apply(t.leaf));
		    else
			return nodeMap.apply(t);
		})
	    .collect(Collectors.toCollection(ArrayList::new));

	for(int i = mapped.size() - 1; i >= 0; i--){
	    if( !res.get(i).isSingleton() )
		concat.accept(mapped.get(i),childsPos.get(i)
			      .stream()
			      .map(mapped::get)
			      .collect(Collectors.toCollection(ArrayList::new)));
	}
	
	return mapped.get(0);
    }

    /*public <V,W> Tree<V,W> totalMap(Function<T,V> leafMap, BiFunction<U,List<Tree<V,W>>,Tree<V,W>> nodeMap){
	if( singleton() )
	    return new Tree<>( leafMap.apply(leaf) );
      	else
	    return nodeMap.apply(node,childsMap( t -> t.totalMap(leafMap,nodeMap) ));
	    }*/

    /*public <V,W> Tree<V,W> totalMap(Function<T,V> leafMap, BiFunction<U,List<Tree<V,W>>,Tree<V,W>> nodeMap){
	if( singleton() )
	    return new Tree<>( leafMap.apply(leaf) );
      	else
	    return nodeMap.apply(node,childsMap( t -> t.totalMap(leafMap,nodeMap) ));
	    }*/
    
    public <V> Tree<U,V> leafMap(Function<T,V> map){
	return map(map,x-> new Tree<>(x.node,null), (t,c) -> {t.childs = c;} );
    }

    public void extendLeaf(Function<T,Tree<U,T>> map){
	Stack<Tree<U,T>> wait = new Stack<>();
	wait.push(this);

	while( !wait.isEmpty() ){
	    Tree<U,T> parent = wait.pop();
	    for(int i = 0; i < parent.childs.size(); i++){
		Tree<U,T> cur = parent.childs.get(i);
		if( cur.isSingleton() )
		    parent.childs.set(i,map.apply(cur.leaf));
		else
		    wait.push(cur);
	    }
	}
    }

    public void absorbChilds(Predicate<Tree<U,T>> p){
	childs = childs.stream()
	    .flatMap( t -> (!t.isSingleton() && p.test(t)) ? t.childs.stream() : Stream.of(t) )
	    .collect( Collectors.toCollection(ArrayList::new) );
    }

    /** fait de t un enfants de this */
    public void addChild(Tree<U,T> t){
	childs.add(t);
    }

    public void resetChilds(){
	childs = new ArrayList<>();
    }

    /** renvoi les enfants de this */
    public Iterable<Tree<U,T>> childs(){
	return childs;
    }

    /** stream parallel */
    public <W> List<W> childsMap(Function<Tree<U,T>,W> map){
	return childs.parallelStream().map(map).collect(Collectors.toCollection(ArrayList::new));
    }

    public Stream<Tree<U,T>> childsStream(){
	return childs.stream();
    }

    /** renvoi l'enfant à la position i */
    public Tree<U,T> child(int i){
	return childs.get(i);
    }

    /** renvoi le nombre d'enfants de this */
    public int degree(){
	return childs != null ? childs.size() : 0;
    }

    /** renvoi si this n'a pas d'enfant */
    public boolean isEmpty(){
	return childs == null || childs.isEmpty();
    }

    /** test une propriété sur les etiquettes des noeuds */
    public static <T,U> boolean compareNodes(Tree<U,T> t1, Tree<U,T> t2, BiPredicate<U,U> p){
	return !t1.isSingleton() && !t2.isSingleton() && p.test(t1.node,t2.node);
    }

    /** this et t sont fusioné : this prend tous les enfants de t */
    public void fusion(Tree<U,T> t){
	childs.addAll(t.childs);
    }

    public void setNode(U t){
	node = t;
    }
    public U getNode(){ return node; }

    public boolean isNodeAnd(Predicate<U> p){
	return !isSingleton() && p.test(node);
    }

    /** renvoi un élément décendant de this */
    public T getLeaf(){
	if( isSingleton() )
	    return leaf;
	for(Tree<U,T> t : childs){
	    T tmp = t.getLeaf();
	    if( tmp != null )
		return tmp;
	}
	throw new NoSuchElementException();
    }

    /** renvoi tous les éléments décendant de this */
    public Set<T> getAllLeaf(){
	if( isSingleton() )
	    return Set.of(leaf);
	
	final Set<T> res = new HashSet<>();
	for(Tree<U,T> t : this.decendant( (x) -> x.isSingleton() ))
	    res.add(t.getLeaf());
	return res;
    }

    /** renvoi le dernier ancetre commun de x et y */
    public Optional<Tree<U,T>> ancestor(T x, T y){
	if( isSingleton() )
	    return Optional.empty();
	
	boolean seenX = false;
	boolean seenY = false;
	for(Tree<U,T> t : childs){
	    Set<T> base = t.getAllLeaf();
	    if( base.contains(x) && base.contains(y) )
		return t.ancestor(x,y);

	    seenX = seenX || base.contains(x);
	    seenY = seenY || base.contains(y);
	    
	    if( seenX && seenY )
		return Optional.of(this);
	}
	return Optional.empty();
    }

    /** trie les enfants de this selon c */
    public void sort(Comparator<Tree<U,T>> c){
	childs.sort​(c);
    }

    /** parcours tous les décendant de this, this compris */
    public Iterable<Tree<U,T>> decendant(Predicate<Tree<U,T>> isValid){
	return Tool.chooseIteration(decendant(),isValid);
    }
    public Iterable<Tree<U,T>> decendant(){
	final Tree<U,T> base = this;
	return new Iterable<>(){
   
	    public Iterator<Tree<U,T>> iterator(){
		final Stack<Tree<U,T>> dummy = new Stack<>();
		dummy.add(base);
		return new Iterator<>(){
		    
		    Stack<Tree<U,T>> current = dummy;
		    
		    public boolean hasNext(){
			return !current.isEmpty();
		    }
		    
		    public Tree<U,T> next(){
			Tree<U,T> res = current.pop();
			if( !res.isSingleton() )
			    current.addAll(res.childs);
			return res;
		    }
		    
		};
	    }
	};
    }

    public Iterator<Tree<U,T>> iterator(){
	return decendant().iterator();
    }

    public Stream<Tree<U,T>> stream(){
	Stream.Builder<Tree<U,T>> build = Stream.builder();
	Stack<Tree<U,T>> wait = new Stack<>();
	wait.push(this);

	while( !wait.isEmpty() ){
	    Tree<U,T> cur = wait.pop();
	    build.add(cur);
	    if( !cur.isSingleton() )
		wait.addAll(cur.childs);
	}

	return build.build();
    }
    
    public String toString(){ return toString(""); } 
    public String toString(String prefix){
	if( isSingleton() )
	    return "─ " + leaf + "\n";
	
	String label = node.toString();
	StringBuilder res = new StringBuilder(label);
	
	String prefix_base = prefix + Tool.wordToSpace(label);
	String prefix_some   = prefix_base + "│";
	String prefix_middle = prefix_base + "├";
	String prefix_end    = prefix_base + "└";
	String prefix_none   = prefix_base + " ";

	if( childs.size() == 0 ){
	    res.append("─╳\n");
	}
	else if( childs.size() == 1 ){ /** ne devrait pas arriver */
	    res.append('─').append( childs.get(0).toString( prefix_none ) );
	}else{   
	    //0
	    res.append('┬').append( childs.get(0).toString( prefix_some ) );
	    
	    for(int i = 1; i < childs.size() - 1; i++)
		res.append(prefix_middle).append(childs.get(i).toString(prefix_some));
	    
	    //last 
	    res.append(prefix_end).append(childs.get(childs.size() - 1).toString(prefix_none));
	}
	return res.toString();
    }
}

    
