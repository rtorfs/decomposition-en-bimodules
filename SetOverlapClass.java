import java.util.Set;
import java.util.HashSet;

import java.util.HashMap;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Stack;

import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import java.util.function.Predicate;
import java.util.function.IntUnaryOperator;

public class SetOverlapClass{

    private static HashMap<IntSet,IntSet> computeMax(List<IntSet> l){
	HashMap<IntSet,IntSet> res = new HashMap<>();

	

	return res;
    }

    public static Set<IntSet> resolve2(Set<IntSet> s){
	List<IntSet> sets = new ArrayList<>(s);

	return null;
    }

    /**
     * prend en entré un ensemble d'ensemble 
     * renvoi un ensemble où les classes de chevauchement sont fusionées
     */
    // TODO : optimiser
    public static Set<IntSet> resolve(Set<IntSet> s){
	InterfaceGraph<IntSet,Graph> overlap = InterfaceGraph.ofBase(s, (x,y) -> x.inclusionType(y) == IntSet.Inclusion.OVERLAP);

	Vector<Set<IntSet>> comp = overlap.computeSets( StronglyConnectedComponents::compute );
	return comp.stream()
	    .map( x -> new IntSet( x.stream().flatMapToInt(IntSet::stream) ) )
	    .collect( Collectors.toUnmodifiableSet() );
    }

    /** 
     * suppose que l est ordoné par taille, 
     * sans chevauchement,
     * contient une racine 
     * et des éléments entre [0;maxElem[ 
     */
    static <T extends IntSet> Tree<T,T> inclusionTree(int maxElem, List<T> l){
	Vector<Stack<Tree<T,T>>> matrix = new Vector<>(maxElem);
	IntStream.range(0,maxElem)
	    .mapToObj( i -> new Stack<Tree<T,T>>() )
	    .forEach( matrix::add );

	Vector<Tree<T,T>> trees = new Vector<>();
	for(T ts : l){
	    Tree<T,T> tt = Tree.withChilds(ts);
	    for(int x : ts)
		matrix.get(x).add(tt);
	    trees.add(tt);
	}

	ListIterator<T> ite = l.listIterator(l.size());
	while( ite.hasPrevious() ){
	    int i = ite.previousIndex();
	    if( i == 0 )
		break;
	    T s = ite.previous();
	    s.stream().mapToObj( matrix::get ).forEach( Stack::pop );
	    Tree<T,T> t = trees.get(i);
	    if( t.degree() == 0 ) //pas de sous-ensemble
		t = new Tree<T,T>(s);
	    
	    int x = s.min();
	    Tree<T,T> pere = matrix.get(x).peek();
	    pere.addChild(t);
	}
	
	return trees.get(0);
    }

    /** suppose tous se chevauchent ou disjoints et pas singletons */
    public static Stream<IntSet> resolveKill(Set<IntSet> s){
	IntSet bad = new IntSet( Tool.cartesianProduct(s.stream(),s::stream)
				 .filter( p -> p.fst != p.snd )
				 .map( p -> p.fst.inter(p.snd) )
				 .map( IntSet::stream )
				 .flatMap( IntStream::boxed )
				 .collect( Collectors.toUnmodifiableSet() ));

	
	return s.stream()
	    .map( i -> i.setMinus(bad) )
	    .filter( i -> i.size() > 0 );
    }


    private static boolean isBicolor(TriGraph g, IntSet s){
	return s.stream().mapToObj(g::colorOf)
	    .reduce( (x,y) ->{
		    switch(x){
		    case WHITE:
			if( y == TriGraph.Color.WHITE )
			    return TriGraph.Color.WHITE;
			else
			    return TriGraph.Color.BICOLOR;
		    case BLACK:
			if( y == TriGraph.Color.BLACK )
			    return TriGraph.Color.BLACK;
			else
			    return TriGraph.Color.BICOLOR;
		    default:
			return TriGraph.Color.BICOLOR;
		    }
		})
	    .orElse(TriGraph.Color.BLACK) == TriGraph.Color.BICOLOR;
    }
    public static Set<IntSet> resolveBicolor(Set<IntSet> s, TriGraph g){
	InterfaceGraph<IntSet,Graph> biOverlap = InterfaceGraph.ofBase(s, (x,y) ->
								       isBicolor(g, x.inter(y) )
								       );

	Vector<Set<IntSet>> comp = biOverlap.computeSets( StronglyConnectedComponents::compute );
	Set<IntSet> res = comp.stream()
	    .map( x -> new IntSet( x.stream().flatMapToInt(IntSet::stream) ) )
	    .collect( Collectors.toUnmodifiableSet() );

	return resolveKill(res)
	    .filter( i -> i.stream().mapToObj(g::isBicolor).mapToInt(x -> x ? 2 : 1).sum() >= 3 ) //on enléve les classes avec moins de 3 sommets
	    .collect( Collectors.toUnmodifiableSet() );
    }
}
