import java.util.function.IntFunction;
import java.util.function.Consumer;

import java.io.PrintStream;
import java.io.DataOutput;
import java.io.DataOutputStream​;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.File;
import java.nio.file.Files;

import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import java.util.Arrays;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
public class Main{

    static class SizeCounter implements DataOutput{

	static int computeSize(Consumer<DataOutput> op) throws IOException{
	    SizeCounter sc = new SizeCounter();
	    op.accept(sc);
	    return sc.getSize();
	}

	int size;

	int getSize(){
	    return size;
	}

	void reset(){
	    size = 0;
	}

	SizeCounter(){
	    size = 0;
	}
	
	public void write​(byte[] b){
	    size = size + b.length;
	}
	
	public void write​(byte[] b, int off, int len){
	    throw new UnsupportedOperationException();
	}
	    
	public void write​(int b){
	    throw new UnsupportedOperationException();
	}
	
	public void writeBoolean​(boolean v){
	    size = size+1;
	}
	
	public void writeByte​(int v){
	    size = size+1;
	}
	
	public void writeBytes​(String s){
	    throw new UnsupportedOperationException();
	}
	
	public void writeChar​(int v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeChars​(String s){
	    throw new UnsupportedOperationException();
	}
	
	public void writeDouble​(double v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeFloat​(float v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeInt​(int v){
	    size = size + 4;
	}
	
	public void writeLong​(long v){
	    size = size + 8;
	}
	
	public void writeShort​(int v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeUTF​(String s){
	    throw new UnsupportedOperationException();
	}
    }
    
    static class Log{

	long birth = 0;
	long time = -1;

	PrintStream[] printers;

	Log(){
	    birth = System.currentTimeMillis();
	    printers = new PrintStream[]{ System.err };
	}

	Log(String name, boolean silence){
	    this();
	    try{
		if( silence )
		    printers = new PrintStream[]{ new PrintStream(name) };
		else
		    printers = new PrintStream[]{ new PrintStream(name) , System.err };
	    }catch(Exception e){
		throw new RuntimeException();
	    }
	}

	void print(String s){
	    for(PrintStream p : printers)
		p.print(s);
	}

	void println(String s){
	    for(PrintStream p : printers)
		p.println(s);
	}
	
	void printMaxMemory(){
	    this.println("maximum memory:\t" + octetReadable(maxMemory()) );
	}

	void startChrono(){
	    time = System.currentTimeMillis();
	}

	void printChrono(String msg){
	    long end = System.currentTimeMillis();
	    this.println(msg + miliReadable(end - time));
	}

	void messageAge(String msg){
	    long end = System.currentTimeMillis();
	    this.print("["+miliReadable(end - birth)+"]\t");
	    this.println(msg);
	}
	
	private final static String[] UNITS = {"o","kio","Mio","Gio","Tio"};
	static String octetReadable(long l){
	    if( l == Long.MAX_VALUE )
		return "infinity";
	    int cur = 0;
	    long res = l;
	    while( cur < UNITS.length && res >= 1024 ){
		res = res / 1024;
	    cur++;
	    }
	    return Long.toString(res) + UNITS[cur];
	}
	static long maxMemory(){
	    return Runtime.getRuntime().maxMemory();
	}

	private static final long SECONDS = 1000;
	private static final long MINUTES = SECONDS * 60;
	private static final long HOURS = MINUTES * 60;
	private static final long DAYS = HOURS * 24;
	static String miliReadable(long l){
	    long[] times = {
		l % SECONDS,
		(l % MINUTES) / SECONDS,
		(l % HOURS) / MINUTES,
		(l % DAYS) / HOURS,
		l / DAYS
	    };
	    String[] format = {
		"." + ((times[0] /100)) % 10 + "" + ((times[0] /10) % 10) + "" + (times[0] % 10) + "s",
		"" + times[1],
		"" + times[2] + "m",
		"" + times[3] + "h",
		"" + times[4] + " days "
	    };
	    int cur = format.length - 1;
	    for(; cur >= 0; cur--){
		if( times[cur] > 0 )
		    break;
	    }
	    cur = cur < 1 ? 1 : cur;
	    String res = "";
	    for(int i = cur; i >= 0 && i >= cur-3 ; i--){
		res = res + format[i];
		if( 1 == cur-3 && i == 1 )
		    res = res + "s";
	    }
	    return res;
	}
	
    }

    static void modularDecomposition(TriGraph g, IntFunction<String> verticeName){	
	System.out.println("n=" + g.size() +"\tm="+ g.arcSize());
	
	log.startChrono();
	Tree<TriModule.Data,Integer> t = TriModule.decomposition(g);
	log.printChrono("time to decompose:\t");

	System.out.println( t.leafMap( v -> verticeName.apply(v) ) );
    }

    static void compareCompression(TriGraph g) throws IOException{
	Tree<TriModule.Data,Integer> decomp = TriModule.decomposition(g);
	log.messageAge("decomposition done");

	double oriSize = 1.0 * SizeCounter.computeSize( sc -> {
		try{
		    g.writeOn(sc);
		}catch(IOException e){}
	    });

	double compSize = 1.0 * SizeCounter.computeSize( sc -> {
		try{ TriModule.writeTo(decomp,g::colorOf,true,sc); }
		catch(IOException e){}
	    });
	
	double partSize = 1.0 * SizeCounter.computeSize( sc -> {
		try{ TriModule.writeTo(decomp,g::colorOf,false,sc); }
		catch(IOException e){}
	    });

	System.out.println("compression ratio:\t"+ (compSize / oriSize));
	System.out.println("renamed compression ratio:\t"+ (partSize / oriSize));
    }

    // name, vertice, arc, size, renu_size, ori_size
    static void computeCSVData(String name, TriGraph g) throws IOException{
	Tree<TriModule.Data,Integer> decomp = TriModule.decomposition(g);
	log.messageAge("decomposition done");
	
	long oriSize = SizeCounter.computeSize( sc -> {
		try{ g.writeOn(sc);
		}catch(IOException e){}
	    });
	
	long compSize = SizeCounter.computeSize( sc -> {
		try{ TriModule.writeTo(decomp,g::colorOf,true,sc);
		}catch(IOException e){}
	    });
	
	long partSize = SizeCounter.computeSize( sc -> {
		try{ TriModule.writeTo(decomp,g::colorOf,false,sc);
		}catch(IOException e){}
	    });

	System.out.println(name+","+
			   g.size()+","+
			   g.arcSize()+","+
			   compSize+","+
			   partSize+","+
			   oriSize);
    }
	
    static IntFunction<String> printer;
    static Log log = new Log();

    static InterfaceGraph<String,BiGraph> bigraphPreparator(String name, boolean fromIntegers){
	InterfaceGraph<String,BiGraph> g = fromIntegers ?
	    BiGraph.ofPaires( Reader.integerArcs(name) , x -> "r-"+x, x -> "l-"+x ) :
	    BiGraph.ofPaires( Reader.stringArcs(name) );
	
	return g.apply( BiGraph::noUnIsolated ).apply( BiGraph::noJumeau );
    }

    static Graph graphPreparator(String name){
	ModGraph res = new ModGraph();
	ModGraph rev = new ModGraph();
	Reader.integerArcs(name).forEach( p -> {
		res.add(p);
		rev.add(p.rev());
	    });
	List<Integer> w = new ArrayList<>();
	List<Integer> g = new ArrayList<>();
	List<Integer> b = new ArrayList<>();

	IntStream.range(0,res.size()).forEachOrdered( x -> {
		if( res.degree(x) == 0 )
		    w.add(x);
		else if( rev.degree(x) == 0 )
		    b.add(x);
		else
		    g.add(x);
	    });
	int[] map = new int[res.size()];
	for(int i = 0; i < w.size(); i++)
	    map[w.get(i)] = i;
	for(int i = 0; i < g.size(); i++)
	    map[g.get(i)] = i + w.size();
	for(int i = 0; i < b.size(); i++)
	    map[b.get(i)] = i + w.size() + g.size();
	
	res.reorder( i -> map[i] );
	rev.reorder( i -> map[i] );

	Graph gg = res.toGraph();
	Graph r = rev.toGraph();

	int wgLimit = w.size();
	int gbLimit = w.size() + g.size();
	Set<Integer> toRemove = new HashSet<>();
	
	List<Integer> lw = IntStream.range(0,wgLimit)
	    .boxed()
	    .sorted( (i,j) -> r.voisins(i).compareTo(r.voisins(j)) )
	    .collect( Collectors.toList() );
	for(int i = 1; i < lw.size(); i++)
	    if( r.voisins( lw.get(i-1) ).equals( r.voisins(lw.get(i)) ) )
		toRemove.add( lw.get(i) );
	
	List<Integer> lb = IntStream.range(gbLimit,g.size())
	    .boxed()
	    .sorted( (i,j) -> gg.voisins(i).compareTo(gg.voisins(j)) )
	    .collect( Collectors.toList() );
	for(int i = 1; i < lb.size(); i++)
	    if( gg.voisins( lb.get(i-1) ).equals( gg.voisins(lb.get(i)) ) )
		toRemove.add( lb.get(i) );
	
	return gg.filter( x -> !toRemove.contains(x) ).g;
    }

    static void testOrient(String name) throws IOException{
	Graph g = graphPreparator(name);
	log.println("modular decomposition");
	computeCSVData(name, TriGraph.Immutable.ofGraph(g,true));
	System.out.println("number of strong module = " + TriModule.decomposition( TriGraph.Immutable.ofGraph(g,true) ).size());
	log.println("modular decomposition modification");
	TriGraph color = TriGraph.Immutable.ofGraph(g,false);
	computeCSVData(name, color);
	System.out.println("number of strong trimodule = " + TriModule.decomposition( color ).size());
	log.println("w="+color.whiteSize()+"\t"+"g="+color.bicolorSize()+"\t"+"b="+color.blackSize());
    }

    static IntFunction<String> readGraph(TriGraph[] res, String[] args, int i) throws IOException, FileNotFoundException{
	InterfaceGraph<String,BiGraph> g;
	
	switch( i < args.length ? args[i] : "test1" ){

	case "test1":
	    res[0] = testGraph1();
	    return Integer::toString;

	case "test2":
	    res[0] = testGraph2();
	    return Integer::toString;
	    
	case "file":
	    if( i+1 < args.length ){
		g = bigraphPreparator(args[i+1], true);
		res[0] = new TriGraph.Immutable(g.g);
		return g.vertices::get;
	    }else
		break;

	case "compressed_file":
	    if( i+1 < args.length ){
		try ( DataInputStream dt = new DataInputStream(new FileInputStream(args[i+1])) ) {
		    res[0] = new TriGraph.ModuleTree( TriModule.readFrom(dt) );
		}
		return Integer::toString;
	    }else
		break;
	}

	throw new IllegalArgumentException();
    }

    private static void test() throws IOException{
	File[] gras = new File("./graphs/")
	    .listFiles​(f -> f.getName().endsWith​(".graph") );
	for(File f : gras){
	    TriGraph g = new TriGraph.ModuleTree( TriModule.readFrom(new DataInputStream(new FileInputStream(f))) );
	    
	    Files.writeString(new File(f.getCanonicalPath() + ".bip").toPath(), toBip(g));
	}
    }

    private static class ICGen{
	private int color;

	ICGen(){
	    color = 0;
	}

	synchronized Tree<TriModule.Data,TriModule.IntegerColor> gen(TriGraph.Color c){
	    int x = color;
	    color++;
	    return new Tree<>(new TriModule.IntegerColor(x,c));
	}
	
	Tree<TriModule.Data,TriModule.IntegerColor> white(){
	    return gen(TriGraph.Color.WHITE);
	}
	
	Tree<TriModule.Data,TriModule.IntegerColor> black(){
	    return gen(TriGraph.Color.BLACK);
	}

	Tree<TriModule.Data,TriModule.IntegerColor> ksBase(TriGraph.Color fst, TriGraph.Color snd, int len){
	    List<Tree<TriModule.Data,TriModule.IntegerColor>> l = new ArrayList<>();
	    for(int i = 0; i < len; i++)
		l.add( gen( (i%2==0) ? fst : snd ) );
	    return Tree.withChilds(TriModule.Data.DATA_KS,l);
	}

	Tree<TriModule.Data,TriModule.IntegerColor> ksBaseNoUni(int len){
	    if( len % 2 == 0 )
		return ksBase(TriGraph.Color.WHITE,TriGraph.Color.BLACK,len);
	    else
		throw new IllegalArgumentException();
	}

	Tree<TriModule.Data,TriModule.IntegerColor> ksBaseNoIso(int len){
	    if( len % 2 == 0 )
		return ksBase(TriGraph.Color.BLACK,TriGraph.Color.WHITE,len);
	    else
		throw new IllegalArgumentException();
	}
    }

    private static TriGraph testGraph1(){
	TriModule.Data ser = TriModule.Data.DATA_SERIE;
	TriModule.Data para = TriModule.Data.DATA_PARALLEL;
	ICGen vert = new ICGen();
	Tree<TriModule.Data,TriModule.IntegerColor> res =
	    Tree.withChilds(ser,
			    vert.ksBaseNoUni(28),
			    Tree.withChilds(para,
					    vert.ksBaseNoIso(4),
					    vert.ksBaseNoIso(100),
					    vert.ksBaseNoIso(4),
					    vert.ksBaseNoIso(4),
					    vert.ksBaseNoIso(4),
					    vert.ksBaseNoIso(4)),
			    vert.ksBaseNoUni(18),
			    vert.ksBaseNoUni(8),
			    vert.ksBaseNoUni(12),
			    vert.ksBaseNoUni(4)
			    );

	return new TriGraph.ModuleTree(res);
    }

    private static TriGraph testGraph2(){
	TriModule.Data ser = TriModule.Data.DATA_SERIE;
	TriModule.Data para = TriModule.Data.DATA_PARALLEL;
	ICGen vert = new ICGen();
	Tree<TriModule.Data,TriModule.IntegerColor> res =
	    Tree.withChilds(ser,
			    Tree.withChilds(para,
					    Tree.withChilds(ser,
							    vert.ksBaseNoUni(4),
							    vert.ksBaseNoUni(100),
							    vert.ksBaseNoUni(4)),
					    Tree.withChilds(ser,
							    vert.ksBaseNoUni(4),
							    vert.ksBaseNoUni(100),
							    vert.ksBaseNoUni(4))
					    ),
			    Tree.withChilds(para,
					    Tree.withChilds(ser,
							    vert.ksBaseNoUni(12),
							    vert.ksBaseNoUni(10),
							    vert.ksBaseNoUni(20)),
					    Tree.withChilds(ser,
							    vert.ksBaseNoUni(4),
							    vert.ksBaseNoUni(10),
							    vert.ksBaseNoUni(8)),
					    Tree.withChilds(ser,
							    vert.ksBaseNoUni(16),
							    vert.ksBaseNoUni(14),
							    vert.ksBaseNoUni(10)),
					    vert.ksBaseNoIso(20)),
			    vert.ksBaseNoUni(110)
			    );

	return new TriGraph.ModuleTree(res);
    }

    private static String toBip(TriGraph g){
	return g.blackVertices()
	    .mapToObj( x -> 
		       g.whiteVoisinsOf(x).stream()
		       .mapToObj(Integer::toString)
		       .collect(Collectors.joining​(" ",""," -1\n")) )
	    .collect( Collectors
		      .joining​("",
			       ""+g.blackSize()+" "+g.whiteSize()+"\n",
			       "")
		      );
    }
    
    public static void main(String[] args) throws IOException{
	if( args.length < 1 )
	    throw new IllegalArgumentException();

	if( args[0].equals( "directed_data" ) ){
	    if( args.length < 2 )
		throw new IllegalArgumentException();
	    testOrient(args[1]);
	    return;
	}
	
	log = new Log("decomposition.log",false);
	log.printMaxMemory();

	TriGraph[] g = { null };
	IntFunction<String> verticeName = null;
	
	log.startChrono();
	verticeName = readGraph(g, args, 1);
	log.printChrono("time to load:\t");
	
	printer = verticeName;

	switch( args[0] ){
	case "decompose":
	    modularDecomposition(g[0], verticeName);
	    return;
	case "compress":
	    try (DataOutputStream dt = new DataOutputStream​(System.out)) {
		TriModule.writeTo(g[0],true,dt);
	    }
	    return;
	case "compression":
	    compareCompression(g[0]);
	    return;
	case "csv_data":
	    computeCSVData(args[args.length-1],g[0]);
	    return;
	}
	throw new IllegalArgumentException();
    }

}
