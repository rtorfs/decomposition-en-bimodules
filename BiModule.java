import java.util.Set;
import java.util.HashSet;

import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Comparator;

import java.util.function.Predicate;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.IntFunction;
import java.util.function.Supplier;

import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Collectors;
public class BiModule{

    private BiGraph g;
    private BiGraph rg;
    
    private BiModule(BiGraph g){
	this.g = g;
    }

    static class Data{

	static enum Type { KS , SERIE , PARALLEL , PRIME }
	static enum Color{ BLACK, WHITE, BICOLOR }
	
	Type type;
	Graph whiteToBlack;
	Graph blackToWhite;
	Color[] color = null;

	Data(Type t){
	    type = t;
	    whiteToBlack = null;
	    blackToWhite = null;
	}

	Data(BiGraph g, List<IntSet> l){
	    this(Type.PRIME);
	    setQuotient(computeQuotient(g,l));
	    
	    color = l.stream()
		.map( x -> {
		      if( g.isWhite(x.min()) )
			  if( !g.isWhite(x.max()) )
			      return Color.BICOLOR;
			  else
			      return Color.WHITE;
		      else
			  if( !g.isWhite(x.max()) )
			      return Color.BLACK;
			  else
			      throw new IllegalStateException();
		    })
		.toArray(Color[]::new);
	}

	public Color colorOf(int a){
	    return color[a];
	}
	public boolean comparable(int a, int b){
	    switch(colorOf(a)){
	    case WHITE: return !isBlack(b);
	    case BLACK: return !isWhite(b);
	    default: return true;
	    }
	}

	public boolean isWhite(int x){
	    return color[x] == Color.WHITE;
	}
	public boolean isBlack(int x){
	    return color[x] == Color.BLACK;
	}
	public boolean isBicolor(int x){
	    return color[x] == Color.BICOLOR;
	}
	public boolean isBicolor(IntSet s){
	    boolean seenB = false;
	    boolean seenW = false;
	    for(int x : s){
		switch(colorOf(x)){
		case WHITE: seenW = true; break;
		case BLACK: seenB = true; break;
		case BICOLOR: return true;
		}
		if( seenB && seenW )
		    return true;
	    }
	    return false;
	}
	public int verticeValue(int x){
	    switch( colorOf(x) ){
	    case BLACK: return 1;
	    case BICOLOR: return 0;
	    case WHITE: return -1;
	    }
	    throw new IllegalStateException();
	}
	public int verticeSize(int x){
	    return isBicolor(x) ? 2 : 1;
	}
       
	public IntStream vertices(){
	    return whiteToBlack.vertices();
	}
	
	@Override
	public String toString(){
	    switch(type){
	    case KS: return "K+S";
	    case SERIE: return "series";
	    case PARALLEL: return "parallel";
	    case PRIME: return "prime";
	    default: throw new IllegalStateException();
	    }   
	}

	public boolean isKS(){
	    return type == Type.KS;
	}

	public boolean isPrime(){
	    return type == Type.PRIME;
	}

	private void setQuotient(Graph whiteToBlack){
	    this.whiteToBlack = whiteToBlack;
	    this.blackToWhite = whiteToBlack.reverse();
	}

	public Graph getWB(){
	    return whiteToBlack;
	}

	public Graph getBW(){
	    return blackToWhite;
	}

	/** g et une liste des modules maximaux */
	public static Graph computeQuotient(BiGraph g, List<IntSet> l){
	    ModGraph whiteToBlack = new ModGraph(l.size());

	    IntStream withWhite =
		IntStream.range(0,l.size())
		.filter( i -> g.isWhite(l.get(i).min()) );
	    Supplier<IntStream> withBlack = () ->
		IntStream.range(0,l.size())
		.filter( i -> !g.isWhite(l.get(i).max()) );
	    
	    Tool.cartesianProduct(withWhite, withBlack)
		.filter( p -> p.fst != p.snd )
		.filter( p -> g.isVoisin( l.get(p.fst).min() , l.get(p.snd).max() ) )
		.forEachOrdered( whiteToBlack::add );

	    return whiteToBlack.toGraph();
	}
    }

    static class TypedSet extends IntSet{
	Data.Type type;

	TypedSet(int x){
	    super(x);
	    type = Data.Type.KS;
	}

	TypedSet(int start, int end){
	    super(start,end);
	    type = Data.Type.KS;
	}

	TypedSet(Data.Type t, IntSet s){
	    super(s);
	    type = t;
	}

	TypedSet(Data.Type t, Set<Integer> s){
	    super(s);
	    type = t;
	}

	public static int compare(TypedSet d1, TypedSet d2){
	    int y = Integer.compare(d1.size(),d2.size());
	    if( y != 0 )
		return -y;
	    
	    int x = d1.modLexicoSort(d2);
	    if( x != 0 )
		return x;
	    
	    if( d1.type == Data.Type.KS )
		if( d2.type == Data.Type.KS )
		    return 0;
		else
		    return 1;
	    else
		if( d2.type == Data.Type.KS )
		    return -1;
		else
		    return 0;
	}
    }

    private Stream<TypedSet> nonKSBiModuleAux(IntSet s){
	boolean[] sub = new boolean[g.size()];
	Arrays.fill(sub,false);
	for(int x : s)
	    sub[x] = true;
	
	Vector<Set<Integer>> comp = StronglyConnectedComponents.compute(g);
	if( comp.size() > 1 ){ // parrallel
	    return Stream.concat( Stream.of( new TypedSet(Data.Type.PARALLEL,s) ),
				  comp.stream().map( ss -> new TypedSet(Data.Type.KS,ss) ));
	}else{
	    Vector<Set<Integer>> compcomp = StronglyConnectedComponents.compute(rg);
	    if( compcomp.size() > 1 ){ // serie
		return Stream.concat( Stream.of( new TypedSet(Data.Type.SERIE,s) ),
				      compcomp.stream().map( ss -> new TypedSet(Data.Type.KS,ss) ));
	    }else{
		return Stream.of( new TypedSet(Data.Type.PRIME,s) );
	    }
	}
    }

    /** renvoi les bimodules canonique n'étant pas des K+S, trié par taille puis lexicographiquement */
    private List<TypedSet> nonKSBiModule(){
	Set<IntSet> implClass = CoComparability.allNonTrivialImplicationClass(g);
	implClass = SetOverlapClass.resolve(implClass);
	Main.log.messageAge("overlap of mininam bimodule done");
	/*System.out.println( "test decomposition trimodulaire" );
	System.out.println( CoComparability.implicationClassTree(g)
			    .map(x->x, (x,y) -> x.stream().mapToObj(Graph.mainPrinter).collect( Collectors.toUnmodifiableSet() ) )
			    
			    );*/

	/*Set<IntSet> dummy = implClass;
	Set<IntSet> test = SetOverlapClass.resolve(g.exper());
	System.out.println("tst C imp = " + implClass.containsAll(test) );
	System.out.println("imp C tst = " + test.containsAll(implClass) );
	System.out.println("|imp| = " + implClass.size() + " |tst| = " + test.size());
	
	System.out.println("tst\\imp = ");
	test.stream().filter( x -> !dummy.contains(x) ).forEach(is -> System.out.println(is.toString(Graph.mainPrinter)) );
	System.out.println("imp\\tst = ");
	implClass.stream().filter( x -> !test.contains(x) ).forEach(is -> System.out.println(is.toString(Graph.mainPrinter)) );*/

	rg = g.biComplement();
	List<TypedSet> res = implClass.parallelStream()
	    .flatMap( this::nonKSBiModuleAux )
	    .sorted( TypedSet::compare )
	    .collect(Collectors.toCollection(LinkedList::new));
	rg = null;
	
	Iterator<TypedSet> i = res.iterator();
	if( res.size() > 1 ){
	    TypedSet prev = i.next();
	    while( i.hasNext() ){
		TypedSet cur = i.next();
		if( cur.size() == prev.size()
		    && cur.min() == prev.min() ) //les ensembles sont duppliqué on en supprime 1 (en priorité les K+S)
		    i.remove();
		prev = cur;
	    }
	}

	return res;
    }

    private static IntSet voisinsDiff(Graph g, int a, int b){
	return g.voisins(a).xor(g.voisins(b));
    }

    private static boolean atLeast3(Data d, int a, int b){
	return d.isBicolor(a) || d.isBicolor(b);
    }
    private static boolean atLeast3(Data d, IntSet s){
	return s.stream().map(d::verticeSize).sum() >= 3;
    }

    private static Set<IntSet> allSmallKS(Data dt){
	Graph wtb = dt.whiteToBlack; Graph btw = dt.blackToWhite;
	int[][] onlyDifference = dt.vertices() // oD[a][b] = c ssi c est le seul sommet qui dist a de b, si aucun dist -1, sinon -2
	    .mapToObj( b ->
		       IntStream.range(0,b)
		       .map( a -> {
			       if( !dt.comparable(a,b) )
				   return -1;
			       
			       IntSet differents =
				   voisinsDiff(wtb,a,b).union( voisinsDiff(btw,a,b) )
				   .filter( x -> x != a && x != b );

			       if( differents.size() < 1 )
				   return -1;
			       else if( differents.size() > 1 )
				   return -2;
			       else
				   return differents.min();
			   })
		       .toArray()
		       )
	    .toArray(int[][]::new);
	
	return wtb.vertices().boxed()
	    .flatMap( i -> IntStream.range(0,i).mapToObj( j -> new IntPaire(j,i) ) )
	    .map( p -> {
		    int a = p.fst; int b = p.snd;
		    int diff = onlyDifference[b][a];

		    if( diff == -2 )
			return null;
		    else if( diff == -1 )
			return atLeast3(dt,a,b) ? IntSet.of(a,b) : null;

		    int bd = diff > b ? onlyDifference[diff][b] : onlyDifference[b][diff];
		    int ad = diff > a ? onlyDifference[diff][a] : onlyDifference[a][diff];
		    if( (bd == -1 || bd == a)
			&& (ad == -1 || ad == b) )
			return IntSet.of(a,b,diff);
		    else
			return null;
		})
	    .filter( p -> p != null )
	    .collect( Collectors.toUnmodifiableSet() );
    }

    private static Set<IntSet> smallKSWhithoutTriangleConflict(Set<IntSet> s, Data dt){
	Set<Integer> maybe = new HashSet<>();
	Set<Integer> triInterdit = new HashSet<>();
	
	/*s.stream()
	    .filter( is -> is.size() == 3 )
	    .map( is ->
		  is.stream().boxed()
		  .sorted( Comparator.comparingInt(dt::verticeValue) )
		  .mapToInt(i->i).toArray() )
	    .filter( is -> dt.isWhite(is[0]) )
	    .filter( is -> dt.isBlack(is[2]) )
	    .forEachOrdered( is -> {
		    int dist = -1;
		    switch( dt.colorOf(is[1]) ){
		    case WHITE: dist = is[2]; break;
		    case BLACK: dist = is[0]; break;
		    default: return;
		    }

		    if( maybe.contains(dist) )
			triInterdit.add(dist);
		    else
			maybe.add(dist);
			});*/
	
	return s.stream()
	    .flatMap( mod -> {
		    IntSet dos = mod.filter( x -> !triInterdit.contains(x) );
		    if( atLeast3(dt,dos) )
			return Stream.of( dos );
		    else
			return Stream.empty();
		}).collect( Collectors.toUnmodifiableSet() );
    }

    private static Set<IntSet> modulesKS(Data dt){
	Set<IntSet> sansConflitTriangle = smallKSWhithoutTriangleConflict(allSmallKS(dt),dt);
	return sansConflitTriangle;//SetOverlapClass.resolveBicolor(sansConflitTriangle, dt);
    }

    private static int linearSorting(BiGraph g, int s1, int s2){
	if( g.isWhite(s1) )
	    if( g.isWhite(s2) )
		return Integer.compare(g.degree(s1),g.degree(s2));
	    else
		return g.isVoisin(s1,s2) ? 1 : -1;
	else
	    if( g.isWhite(s2) )
		return g.isVoisin(s1,s2) ? -1 : 1;
	    else
		return -Integer.compare(g.degree(s1),g.degree(s2));
    }

    private Tree<Integer,Data> partialDecomposition(List<TypedSet> l){
	Tree<TypedSet,Integer> root = SetOverlapClass.inclusionTree(g.size(),l).leafMap(IntSet::min);

	/*Tree<Integer,Data> res = root.map(x->x, (ts,chi) -> {
		if( ts.type == Data.Type.PRIME ){
		    List<IntSet> maxmods = chi
			.map( t -> t.singleton() ? new IntSet(t.getBase()) : t.getNode() )
			.collect(Collectors.toCollection(ArrayList::new));
		    return new Data(g, maxmods);
		}else
		    return new Data(ts.type);
		    });*/
	return null;
    }
    
    /** partitionne en bimodules maximaux */
    private static List<IntSet> ksPrimeChild(Data dt){
	Set<IntSet> mods = modulesKS(dt);
	
	Set<Integer> seen = mods.stream()
	    .flatMapToInt( IntSet::stream )
	    .boxed().collect( Collectors.toUnmodifiableSet() );
	
	return Stream.concat( mods.stream() , 
			      dt.vertices()
			      .filter( x -> !seen.contains(x) )
			      .boxed()
			      .map( IntSet::of ) )
	    .collect(Collectors.toCollection(ArrayList::new));
    }
    
    public static Tree<Integer,Data> decomposition(BiGraph g){
	BiModule res = new BiModule(g);
	/*g.vertices().forEach( i -> {
		System.out.println(""+i+" -> "+Graph.mainPrinter.apply(i));
		});*/
	List<TypedSet> partialSet = res.nonKSBiModule();
	Main.log.messageAge("non-K+S bimodules & childs of complete done");

	// V n'est pas détécté i.e. est un K+S
	if( partialSet.isEmpty() || partialSet.get(0).size() < g.size() )
	    partialSet.add(0, new TypedSet(0,g.size()) );
	IntStream.range(0,g.size())
	    .mapToObj( TypedSet::new )
	    .forEach( partialSet::add );
	
	
        Tree<Integer,Data> partialTree = res.partialDecomposition(partialSet);
	Main.log.messageAge("partial tree structure done");

	/*Tree<Integer,Data> finalTree = partialTree.totalMap(x->x, (dt, childs) -> {
		if( dt.isPrime() ){
		    List<Tree<Integer,Data>> newChilds = ksPrimeChild(dt)
			.stream().map( IntSet::stream )
			.map( s -> s.mapToObj(childs::get) )
			.map( s -> s.collect(Collectors.toCollection(ArrayList::new)) )
			.map( l -> l.size() == 1 ? l.get(0) : new Tree<>(new Data(Data.Type.KS),l) )
			.collect(Collectors.toCollection(ArrayList::new));
		    
		    List<IntSet> repre = newChilds.stream()
			.map( Tree::getSet )
			.map( IntSet::new )
			.collect(Collectors.toCollection(ArrayList::new));

		    return new Tree<>(new Data(g,repre),newChilds);
		}
		else
		    return new Tree<>(dt,childs);
		    });*/
	Main.log.messageAge("K+S childs of primes done");

	/*for(Tree<Integer,Data> t : finalTree)
	    if( t.nodePredicate(Data::isKS) )
	    t.sort( (x,y) -> linearSorting(g,x.getBase(),y.getBase()) );*/

	throw new IllegalStateException("ne pas utiliser, non fonctionel");
    }
    
}
