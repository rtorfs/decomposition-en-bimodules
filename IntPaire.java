import java.util.Objects;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
public class IntPaire{
    final int fst;
    final int snd;

    IntPaire(int x, int y){
	fst = x;
	snd = y;
    }

    @Override
    public String toString(){
	return "("+fst+";"+snd+")";
    }
    
    public String toString(IntFunction p){
	return "("+p.apply(fst)+";"+p.apply(snd)+")";
    }

    @Override
    public int hashCode(){
     	return Objects.hashCode(fst) + Objects.hashCode(snd) * 7919;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean equals(Object o){
	if( o != null && o.getClass() == this.getClass() ){
	    IntPaire p = (IntPaire) o;
	    return p.fst == this.fst && p.snd == this.snd;
	}else{
	    return false;
	}
    }

    public Paire<Integer> toPaire(){
	return new Paire<>(fst,snd);
    }

    public IntPaire map(IntUnaryOperator iup){
	return new IntPaire(iup.applyAsInt(fst),iup.applyAsInt(snd));
    }

    public boolean different(){
	return fst != snd;
    }
    
    public static IntPaire smallFirst(IntPaire p){
	return p.fst < p.snd ? p : p.rev();
    }
    public static IntPaire smallFirst(int a, int b){
	return a < b ? new IntPaire(a,b) : new IntPaire(b,a);
    }

    /** (x,y) -> (y,x) */
    public IntPaire rev(){
	return new IntPaire(snd,fst);
    }
}
