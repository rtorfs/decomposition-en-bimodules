import java.util.Objects;
import java.util.Arrays;
import java.util.Iterator;

import java.util.Vector;
import java.util.List;
import java.util.ArrayList;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

import java.util.function.BiPredicate;
import java.util.function.IntPredicate;
import java.util.function.IntFunction;

import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

import java.io.PrintStream;
public class Graph{

    protected final IntSet[] arcs;

    /** renvoi les voisins de x */
    IntSet voisins(int x){
	return arcs[x];
    }
    /** renvoi si il y a un arc de l'élément x vers l'élément y */
    boolean isVoisin(int x, int y){
	return voisins(x).contains(y);
    }

    /** renvoi si la paire est un arc dans le graph */
    boolean isArc(Paire<Integer> p){
	return isVoisin(p.fst,p.snd);
    }
    
    /** renvoi le degré sortant de x */
    int degree(int x){
	return arcs[x].size();
    }

    /** renvoi le nombre de sommet du graphe */
    public int size(){ return arcs.length; }

    /** renvoi le nombre d'arc du graphe */
    public long arcSize(){
	return vertices().mapToLong( this::degree ).sum();
    }

    public String toStringSommet(IntFunction<String> printer, int i){
	return arcs[i].stream().mapToObj( printer )
	    .collect( Collectors.joining("\t", printer.apply(i) + " :\t","") );
    }

    public String toString(IntFunction<String> printer){
	return vertices().mapToObj( i -> this.toStringSommet(printer,i) )
	    .collect( Collectors.joining("\n") );
    }

    @Override
    public String toString(){
	return toString( Integer::toString );
    }

    public IntStream vertices(){
	return IntStream.range(0,size());
    }

    /**
       renvoi un stream sur tous les arcs de this
     */
    public Stream<IntPaire> arcs(){
	return vertices().boxed().flatMap( i ->
					   arcs[i].stream().mapToObj( j -> new IntPaire(i,j) )
					   );
    }

    public Graph(Graph g){
	this.arcs = g.arcs;
    }

    /** un graphe par adjacence de ll */
    public Graph(Stream<IntSet> s){
	arcs = s.toArray( IntSet[]::new );
    }

    public Graph(int size, IntFunction<IntSet> voisins){
	this( IntStream.range(0,size).mapToObj( voisins ) );
    }

    /** un graphe de taille size avec l'arc (x,y) ssi isArc.test(x,y) */
    public Graph(int size, BiPredicate<Integer,Integer> isArc){
	this( IntStream.range(0,size)
	      .mapToObj( i -> IntStream.range(0,size)
			 .filter( j -> i != j )
			 .filter( j -> isArc.test(i,j) )
			 )
	      .map( IntSet::new ));
    }

    /** enleve les sommets qui ne respectent pas isSommet et les arc qui ne respectent pas isArc */
    public InterfaceGraph<Integer,Graph> filter(IntPredicate isSommet, BiPredicate<Integer,Integer> isArc){
	Vector<Integer> name = new Vector<>();
	int[] posOf = new int[size()];
	for(int i = 0; i < arcs.length; i++)
	    if( isSommet.test(i) ){
		posOf[i] = name.size();
		name.add(i);
	    }else
		posOf[i] = -1;
	
	Graph subGraph = new Graph(name.size(),
				   x -> voisins(name.get(x))
				   .filterMap(
					      (y) -> posOf[y] >= 0 && isArc.test(name.get(x),y),
					      (y) -> posOf[y]));
	
	return new InterfaceGraph<>(subGraph,name);
    }
    public InterfaceGraph<Integer,Graph> filter(IntPredicate isSommet){
	return filter(isSommet, (x,y) -> true);
    }
    public InterfaceGraph<Integer,Graph> filter(BiPredicate<Integer,Integer> isArc){
	return InterfaceGraph.identity(new Graph(size(), (x) -> voisins(x).filter( (y) -> isArc.test(x,y) )));
    }

    /**
       renvoie le graphe avec les arcs inversé : (a,b) -> (b,a)
    */
    public Graph reverse(){
        ModGraph res = new ModGraph( size() );
	arcs().map( IntPaire::rev )
	    .forEachOrdered( res::add );
	return res.toGraph();
    }

    /** renvoi si a distingue b de c */
    public boolean distingue(int a, int b, int c){
	return isVoisin(a,b) != isVoisin(a,c)
	    || isVoisin(b,a) != isVoisin(c,a);
    }

    /** renvoi les puit de this */
    public Set<Integer> sinks(){
	return vertices().filter( v -> degree(v) == 0 )
	    .boxed().collect( Collectors.toUnmodifiableSet() );
    }

    /** chaque composente fortement connexe est une vertice et X->Y si il existe x€X et y€Y tel que (x,y)€E */
    public InterfaceGraph<Set<Integer>,Graph> componentGraph(){
	Vector<Set<Integer>> l = StronglyConnectedComponents.compute(this);

	Graph g = new Graph(l.size(), i ->
			    new IntSet(
				       IntStream.range(0,l.size())
				       .filter( j -> i != j )
				       .filter( j ->
						Tool.cartesianProduct( l.get(i).stream() , l.get(j)::stream )
						.anyMatch(this::isArc) )
				       )
			    );
	
	return new InterfaceGraph<>(g,l);
    }

    /** renvoi le sous graphe sans puit */
    /*public Graph<T> withoutSink(){
	return filter( (v) -> degree(v) > 0 , (x,y) -> true );
	}*/

    /** le complement de this */
    /*public Graph<T> complement(){
	return new Graph(arcs.length, (x,y) -> !isVoisin(x,y) );
	}*/

    /** renvoi un graphe où les arcs ont perdus leurs orientations */
    public Graph desorient(){
	return new Graph(arcs.length, (x,y) -> isVoisin(x,y) || isVoisin(y,x) );
    }

    /** un graphe où (x,y)∈E ssi N(x)⊆N(y)*/
    public Graph voisinInclusionGraph(){
	return new Graph(arcs.length, (x,y) -> voisins(y).subSetOf(voisins(x)) );
    }

    /** 
     * créer un graphe à partir d'une decomposition modulaire
     * suppose que decomp contient tous les entiers de 0 à len
     */
    public static Graph ofModularDecomposition(Tree<Module.Data,Integer> decomp){
	ModGraph res = new ModGraph();
	ofModularDecomposition(decomp,res);
	return res.toGraph();
    }
    
    private static void ofModularDecomposition(Tree<Module.Data,Integer> decomp, ModGraph res){
	if( decomp.isSingleton() ){
	    res.add( decomp.getLeaf() );
	}
	else{
	    List<Set<Integer>> childs = decomp.childsMap( Tree::getAllLeaf );
	    IntFunction<IntStream> voisins;
	    final int len = childs.size();
	    
	    switch( decomp.getNode().getType() ){
	    case LINEAR: voisins = x -> IntStream.range(0,x); break;
	    case SERIE: voisins = x -> IntStream.range(0,len).filter( y -> x!=y ); break;
	    case PARALLEL: voisins = x -> IntStream.empty(); break;
	    case PRIME:
		Graph quotient = decomp.getNode().getQuotient();
		voisins = x -> quotient.voisins(x).stream(); break;
	    default:
		throw new IllegalStateException();
	    }

	    IntStream.range(0,len).boxed()
		.flatMap( i -> voisins.apply(i).mapToObj( j -> new IntPaire(i,j) ) )
		.flatMap( p ->
			  Tool.cartesianProduct(childs.get(p.fst).stream(),
						childs.get(p.snd)::stream)
			  ) // les sommets de p.fst vers p.snd
		.forEachOrdered( res::add );
	    
	    for(Tree<Module.Data,Integer> t : decomp.childs())
		ofModularDecomposition(t, res);
	}
    }
    
}
