import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

import java.util.IntSummaryStatistics;

import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.IntPredicate;
import java.util.function.BiFunction;

import java.util.Stack;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;

import java.util.NoSuchElementException;
import java.util.Optional;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
/**
 * un graphe avec 3 type de sommets: blanc,noir,bicolor
 * un sommet est relier en tant que blanc avec un autre sommet en tant que noir
 *
 * un sommet blanc ne peut être qu'en tant que blanc
 * un sommet noir ne peut être qu'en tant que noir
 * un sommet bicolor peut être en tant que noir ou blanc
 *
 * u et v bicolor peuvent être relié avec 
 * u en tant que noir et v en tant que blanc et 
 * u en tant que blanc et v en tant que noir
 * en même temps
 */
public abstract class TriGraph{
    
    public static enum Color{BLACK,WHITE,BICOLOR};

    /** un IntStream contenant tous les sommets u tel que colorOf(u) == Color.WHITE */
    abstract public IntStream whiteVertices();
    
    /** un IntStream contenant tous les sommets u tel que colorOf(u) == Color.BICOLOR */
    abstract public IntStream bicolorVertices();
    
    /** un IntStream contenant tous les sommets u tel que colorOf(u) == Color.BLACK */
    abstract public IntStream blackVertices();

    /** le nombre de sommets du graph */
    abstract public int size();
    /** le nombre de sommets u du graph tel que colorOf(u) == Color.WHITE */
    abstract public int whiteSize();
    /** le nombre de sommets u du graph tel que colorOf(u) == Color.BLACK */
    abstract public int blackSize();
    /** le nombre de sommets u du graph tel que colorOf(u) == Color.BICOLOR */
    abstract public int bicolorSize();

    /** le nombre d'arc du graphe */
    abstract public long arcSize();

    /** les sommets u tel que vertice est relié en tant que noir avec u */
    abstract public IntSet whiteVoisinsOf(int vertice);
    /** les sommets u tel que vertice est relié en tant que blanc avec u */
    abstract public IntSet blackVoisinsOf(int vertice);

    /** 
     * renvoi une paire relevant remplissant la condition si elle existe
     */ 
    //abstract Optional<IntPaire> potentialRelevant(Predicate<IntPaire> condition);

    /** la couleur de vertice */
    abstract public Color colorOf(int vertice);

    /** un IntStream contenant tous les sommets du graphe  
     * suppose que les sommets dans [0;size()[
     */
    public IntStream vertices(){
	return IntStream.concat( whiteVertices(),
				 IntStream.concat(bicolorVertices(),
						  blackVertices()));
    }

    /** stream des arcs (b,w) */
    public Stream<IntPaire> arcs(){
	return vertices().boxed().flatMap( b -> whiteVoisinsOf(b).stream().mapToObj( w -> new IntPaire(b,w) ) );
    }

    /** renvoi un élément blanc/bicolor de set */
    abstract public int getAsWhite(IntSet set);
    abstract public int getAsBlack(IntSet set);

    /** le nombre de sommets tel que vertice est relié d'une façon ou d'une autre avec eux */
    public int degreeOf(int vertice){
	return (int) voisins(vertice).count();
    }
   /** le nombre de sommets tel que vertice est relié en tant que noir avec eux */
    public int whiteDegreeOf(int x){
	return whiteVoisinsOf(x).size();
    }
    /** le nombre de sommets tel que vertice est relié en tant que blanc avec eux */
    public int blackDegreeOf(int x){
	return blackVoisinsOf(x).size();
    }

    /** la relation de voisinage entre a et b
     * a --- b avec a en tant que noir
     * a --- b avec a en tant que blanc
     * a --- b avec a en tant que noir et en tant que blanc
     * a     b non relié
     */
    static enum VoisinageType{NONE,AS_WHITE,AS_BLACK,BOTH}
    public VoisinageType voisinageType(int a, int b){
	if( isWhiteVoisin(a,b) )
	    if( isBlackVoisin(a,b) )
		return VoisinageType.BOTH;
	    else
		return VoisinageType.AS_BLACK;
	else 
	    if( isBlackVoisin(a,b) )
		return VoisinageType.AS_WHITE;
	    else
		return VoisinageType.NONE;
    }

    /** l'ensemble des sommets u utel que voisinageType(x,u) != NONE */
    public IntStream voisins(int x){
	return IntStream.concat( whiteVoisinsOf(x).stream() ,
				 blackVoisinsOf(x).stream() )
	    .distinct();
    }
    /** l'ensemble des sommets u utel que voisinageType(x,u) == AS_BLACK || BOTH */
    public boolean isWhiteVoisin(int a, int b){
	return whiteVoisinsOf(a).contains(b);
    }
    /** l'ensemble des sommets u utel que voisinageType(x,u) == AS_WHITE || BOTH */
    public boolean isBlackVoisin(int a, int b){
	return blackVoisinsOf(a).contains(b);
    }

    public boolean isBicolor(int x){
	return colorOf(x) == Color.BICOLOR;
    }
    public boolean isBlack(int x){
	return colorOf(x) == Color.BLACK;
    }
    public boolean isWhite(int x){
	return colorOf(x) == Color.WHITE;
    }
    public boolean canBeBlack(int x){
	return colorOf(x) != Color.WHITE;
    }
    public boolean canBeWhite(int x){
	return colorOf(x) != Color.BLACK;
    }

    /** les sommets blancs appartiennent à [return.fst;return.snd[ */
    abstract public IntPaire whitesRange();
    abstract public IntPaire blacksRange();
    abstract public IntPaire bicolorRange();

    public <T extends TriGraph> T triComplement(BiFunction<Graph,IntFunction<Color>,T> constr){
	ModGraph res = new ModGraph(size());
	Tool.cartesianProduct(IntStream.concat(whiteVertices(),bicolorVertices()),
			      () -> IntStream.concat(blackVertices(),bicolorVertices()))
	    .filter(IntPaire::different)
	    .filter( p -> !isBlackVoisin(p.fst,p.snd) )
	    .map(IntPaire::rev)
	    .forEachOrdered(res::add);
	return constr.apply(res.toGraph(),this::colorOf);
    }

    public Graph toGraph(){
	return new Graph(size(),this::whiteVoisinsOf);
    }

    /** ecris le trigraphe dans out */
    public void writeOn(DataOutput out) throws IOException{
	Tool.writePositiveLong(out,arcSize()); //out.writeLong(arcSize());
	boolean[] error = {false};
	arcs().forEachOrdered( p -> {
		try{
		    Tool.writePositiveInt(out,p.fst);
		    Tool.writePositiveInt(out,p.snd);
		    //out.writeInt(p.fst);
		    //out.writeInt(p.snd);
		}catch(IOException e){
		    error[0] = true;
		}
	    });
	
	if( error[0] )
	    throw new IOException();
    }

    /** lis le trigraphe depuis in */
    public static <T extends TriGraph> T readFrom(BiFunction<IntFunction<Color>,Stream<IntPaire>,T> constructor, IntFunction<Color> colors, DataInput in) throws IOException{
	long m = Tool.readPositiveLong(in); //in.readLong();

	Stream.Builder<IntPaire> arcs = Stream.builder();
	for(int i = 0; i < m; i++){
	    int b = Tool.readPositiveInt(in);
	    int w = Tool.readPositiveInt(in);
	    //int b = in.readInt();
	    //int w = in.readInt();

	    arcs.accept(new IntPaire(b,w));
	}

	return constructor.apply(colors,arcs.build());
    }

    public String toString(){
	return toString(Integer::toString);
    }
    public String toString(IntFunction<String> printer){
	return vertices().mapToObj( i -> this.toStringSommet(printer,i) )
	    .collect( Collectors.joining("\n") );
    }
    
    public String toStringSommet(IntFunction<String> printer, int i){
	return whiteVoisinsOf(i).stream().mapToObj( printer )
	    .collect( Collectors.joining("\t", printer.apply(i) + " :\t","") );
    }

    public String toStringSommetColor(IntFunction<String> printer, int i){
	String color = "ERROR";
	switch( colorOf(i) ){
	case WHITE: color = "W"; break;
	case BLACK: color = "B"; break;
	case BICOLOR: color = "G"; break;
	}
	return color + " " + printer.apply(i);
    }

    /** il est possible de distinguer x et y 
     * i.e. x et y ne sont pas juste blanc et noir
     */
    boolean isComparable(int x, int y){
	switch(colorOf(x)){
	case BLACK: return colorOf(y) != Color.WHITE;
	case WHITE: return colorOf(y) != Color.BLACK;
	}
	return true;
    }

    /** a distingue x de y */
    boolean distingue(int a, int x, int y){
	return isComparable(x,y)
	    && ( isBlack(x) ||  isBlack(x) || isWhiteVoisin(a,x) == isWhiteVoisin(a,y) )
	    && ( isWhite(x) ||  isWhite(x) || isBlackVoisin(a,x) == isBlackVoisin(a,y) );
    }

    /** liste les paires forcé par a et b
     * le plus petit sommet est positioné en premier
     * a et b bicolores, se forcent ssi eux même et b <-> a ou a -\- b
     * important pour la trivialité de (a,b)
     */
    Stream<IntPaire> forcing(int a, int b){
	return Stream.concat(
			     forcingAsBlack(a,b),
			     forcingAsWhite(a,b))
	    .map(IntPaire::smallFirst)
	    .distinct();
    }
    Stream<IntPaire> forcing(IntPaire p){ return forcing(p.fst,p.snd); }
    
    Stream<IntPaire> forcingAsBlack(int a, int b){
	if( isWhite(a) || isWhite(b) )
	    return Stream.empty();


	IntSet aWithSelfIfBicolor = whiteVoisinsOf(a);
	if( isBicolor(a) )
	    aWithSelfIfBicolor = aWithSelfIfBicolor.union(IntSet.of(a));
	
	IntSet bWithSelfIfBicolor = whiteVoisinsOf(b);
	if( isBicolor(b) )
	    bWithSelfIfBicolor = bWithSelfIfBicolor.union(IntSet.of(b));
	
	return Tool.cartesianProduct( aWithSelfIfBicolor.stream() , bWithSelfIfBicolor::stream )
	    .filter( p ->
		     !isWhiteVoisin(a,p.snd) &&
		     !isWhiteVoisin(b,p.fst)
		     );
    }
    
    Stream<IntPaire> forcingAsWhite(int a, int b){
	if( isBlack(a) || isBlack(b) )
	    return Stream.empty();

	IntSet aWithSelfIfBicolor = blackVoisinsOf(a);
	if( isBicolor(a) )
	    aWithSelfIfBicolor = aWithSelfIfBicolor.union(IntSet.of(a));
	
	IntSet bWithSelfIfBicolor = blackVoisinsOf(b);
	if( isBicolor(b) )
	    bWithSelfIfBicolor = bWithSelfIfBicolor.union(IntSet.of(b));
	
	return Tool.cartesianProduct( aWithSelfIfBicolor.stream() , bWithSelfIfBicolor::stream )
	    .filter( p ->
		     !isBlackVoisin(a,p.snd) &&
		     !isBlackVoisin(b,p.fst)
		     );
    }

    /** a et b forment une paire relevant */
    boolean isRelevant(IntPaire p){ return isRelevant(p.fst,p.snd); }
    boolean isRelevant(int a, int b){
	return forcing(a,b).limit(1).count() > 0;
    }

    public boolean equals(Object o){
	if( o instanceof TriGraph ){
	    TriGraph g = (TriGraph) o;

	    return size() == g.size() && vertices().allMatch​( v -> colorOf(v) == g.colorOf(v) && g.whiteVoisinsOf(v).equals( whiteVoisinsOf(v) ) );
	}else
	    return false;
    }
    
    /** suppose que l'ordre des sommets est blanc -> bicolor -> noir */
    public static class Immutable extends TriGraph{
	final IntSet[] whiteToBlack;
	final IntSet[] blackToWhite;
	
	final int limitWG;
	final int limitGB;

	public IntPaire bicolorRange(){
	    return new IntPaire(limitWG,limitGB);
	}

	public IntPaire whitesRange(){
	    return new IntPaire(0,limitWG);
	}

	public IntPaire blacksRange(){
	    return new IntPaire(limitGB,size());
	}

	public int getAsWhite(IntSet s){
	    int res = s.min();
	    if( isBlack(res) )
		throw new NoSuchElementException();
	    else
		return res;
	}

	public int getAsBlack(IntSet s){
	    int res = s.max();
	    if( isWhite(res) )
		throw new NoSuchElementException();
	    else
		return res;
	}

	public Immutable(BiGraph g){
	    whiteToBlack = g.vertices()
		.mapToObj( x -> g.isWhite(x) ? g.voisins(x) : null )
		.toArray(IntSet[]::new);
	    blackToWhite = g.vertices()
		.mapToObj( x -> !g.isWhite(x) ? g.voisins(x) : null )
		.toArray(IntSet[]::new);

	    limitWG = g.whiteSize();
	    limitGB = g.whiteSize();
	}

	/** noir -> blanc */
	public Immutable(int n, IntFunction<IntSet> wVoisins, IntFunction<IntSet> bVoisins, IntFunction color){
	    //Graph r = g.reverse();
	    whiteToBlack = IntStream.range(0,n)
		.mapToObj( x -> color.apply(x) != Color.BLACK ? bVoisins.apply(x) : null )
		.toArray(IntSet[]::new);
	    
	    blackToWhite = IntStream.range(0,n)
		.mapToObj( x -> color.apply(x) != Color.WHITE ? wVoisins.apply(x) : null )
		.toArray(IntSet[]::new);

	    limitWG = vertices()
		.filter( x -> color.apply(x) != Color.WHITE )
		.findFirst()
		.orElse(n);

	    limitGB = vertices()
		.filter( x -> color.apply(x) == Color.BLACK )
		.findFirst()
		.orElse(n);

	    if( vertices().anyMatch( v ->
				     (!isWhite(v) && v < limitWG) ||
				     (!isBicolor(v) && limitWG <= v && v < limitGB) ||
				     (!isBlack(v) && limitGB <= v) ) )
				     throw new IllegalArgumentException();
	}

	/** (b,w) */
	public static Immutable ofArcs(int n, Stream<IntPaire> g, IntFunction color){
	    ModGraph tmp = new ModGraph(n);
	    ModGraph rtmp = new ModGraph(n);
	    g.forEachOrdered( p -> {
		    tmp.add(p);
		    rtmp.add(p.rev());
		});

	    return new Immutable(n, i -> new IntSet(tmp.voisins(i)), i -> new IntSet(rtmp.voisins(i)), color);
	}

	public static Immutable ofGraph(Graph g, boolean allBicolor){
	    Stream<IntPaire> arcs = g.vertices().boxed()
		.flatMap( i -> g.voisins(i).stream()
			  .mapToObj( j -> new IntPaire(i,j) )
			  );
	    if( allBicolor ){
		return ofArcs(g.size(), arcs, i -> Color.BICOLOR);
	    }
	    Graph r = g.reverse();
	    
	    return ofArcs(g.size(), arcs, i -> g.degree(i) == 0 ? Color.WHITE : r.degree(i) == 0 ? Color.BLACK : Color.BICOLOR);
	}
    
	public Immutable(TriGraph g){
	    if( g instanceof Immutable ){
		Immutable h = (Immutable) g;
		whiteToBlack = h.whiteToBlack;
		blackToWhite = h.blackToWhite;
		
		limitWG = h.limitWG;
		limitGB = h.limitGB;
	    }else{
		this.whiteToBlack = new IntSet[g.size()];
		this.blackToWhite = new IntSet[g.size()];
		
		g.vertices().forEach( i -> {
			this.whiteToBlack[i] = g.colorOf(i) != Color.BLACK ? null : g.blackVoisinsOf(i);
			this.blackToWhite[i] = g.colorOf(i) != Color.WHITE ? null : g.whiteVoisinsOf(i);
		    });

		limitWG = whiteSize();
		limitGB = whiteSize() + bicolorSize();
	    }
	}
	
	public IntStream whiteVertices(){
	    return IntStream.range(0,limitWG);
	}
	public IntStream bicolorVertices(){
	    return IntStream.range(limitWG,limitGB);
	}
	public IntStream blackVertices(){
	    return IntStream.range(limitGB,size());
	}
    
	public int size(){
	    return whiteToBlack.length;
	}
	public int bicolorSize(){
	    return limitGB - limitWG;
	}
	public int whiteSize(){
	    return limitWG;
	}
	public int blackSize(){
	    return size() - limitGB;
	}

	public long arcSize(){
	    return vertices().mapToLong(this::whiteDegreeOf).sum();
	}
	    
	public IntSet whiteVoisinsOf(int b){
	    if( blackToWhite[b] == null )
		return IntSet.EMPTY;
	    else
		return blackToWhite[b];
	}
	
	public IntSet blackVoisinsOf(int w){
	    if( whiteToBlack[w] == null )
		return IntSet.EMPTY;
	    else
		return whiteToBlack[w];
	}
	
	/*Optional<IntPaire> potentialRelevant(Predicate<IntPaire> p){
	    return IntStream.range(0,limitGB).
		mapToObj( i ->
			  IntStream.range(i,limitGB)
			  .mapToObj( j -> new IntPaire(i,j) ))
		.flatMap(x->x)
		.filter(p)
		.filter(this::isRelevant)
		.findAny();
		}*/

	public Color colorOf(int x){
	    if( whiteToBlack[x] == null )
		if( blackToWhite[x] == null )
		    throw new IllegalStateException();
		else
		    return Color.BLACK;
	    else
		if( blackToWhite[x] == null )
		    return Color.WHITE;
		else
		    return Color.BICOLOR;
	}
    }

    public static class ModuleTree extends TriGraph{

	private class BiTree{
	    BiTree parent = null;
	    int place = -1;

	    boolean whiteIsolation = true;
	    boolean blackIsolation = true;

	    List<BiTree> childs = null;

	    TriModule.Data node = null;
	    int leaf = -1;

	    BiTree(TriModule.Data n, BiTree p){
		node = n;
		childs = new ArrayList<>();
		if( p != null )
		    p.addChild(this);
	    }

	    BiTree(int x, BiTree p){
		leaf = x;
		if( p != null )
		    p.addChild(this);
	    }

	    private BiTree getFirst(){
		BiTree res = this;
		while(res.parent != null)
		    res = res.parent;
		return res;
	    }

	    private void addChild(BiTree b){
		b.parent = this;
		b.place = childs.size();

		childs.add( b );
	    }

	    void setIsolation(){
		if( parent != null ){
		    whiteIsolation = parent.whiteIsolation && parent.blackVoisinsLocal(place).limit(1).count() == 0;
		    blackIsolation = parent.blackIsolation && parent.whiteVoisinsLocal(place).limit(1).count() == 0;
		}

		if( !isLeaf() )
		    childs.forEach(BiTree::setIsolation);
	    }

	    boolean isLeaf(){
		return childs == null;
	    }

	    IntStream canBeColor(IntPredicate canBeColor, Function<TriGraph,IntStream> theColor){
		if( isLeaf() )
		    if( canBeColor.test(leaf) )
			return IntStream.of(leaf);
		    else
			return IntStream.empty();
		else
		    return (node.isPrime() ? 
			    IntStream.concat( theColor.apply(node.quotient) , node.quotient.bicolorVertices() ) :
			    IntStream.range(0,childs.size()) )
			.mapToObj( childs::get )
			.flatMapToInt( n -> n.canBeColor(canBeColor,theColor) );
	    }

	    IntStream canBeWhiteLeaf(IntPredicate canBeWhite){
		return canBeColor(canBeWhite,TriGraph::whiteVertices);
	    }

	    IntStream canBeBlackLeaf(IntPredicate canBeBlack){
		return canBeColor(canBeBlack,TriGraph::blackVertices);
	    }

	    IntStream whiteVoisinsLocal(int v){
		switch(node.type){
		case SERIE:
		    return IntStream.concat( IntStream.range(0,v) , IntStream.range(v+1,childs.size()) );
		case PRIME:
		    return node.quotient.whiteVoisinsOf(v).stream();
		case KS:
		    return IntStream.range(v+1,childs.size());
		case PARALLEL:
		    return IntStream.empty();
		}
		throw new IllegalStateException();
	    }

	    IntStream blackVoisinsLocal(int v){
		switch(node.type){
		case SERIE:
		    return IntStream.concat( IntStream.range(0,v) , IntStream.range(v+1,childs.size()) );
		case PRIME:
		    return node.quotient.blackVoisinsOf(v).stream();
		case KS:
		    return IntStream.range(0,v);
		case PARALLEL:
		    return IntStream.empty();
		}
		throw new IllegalStateException();
	    }

	    IntStream colorVoisins(BiFunction<BiTree,Integer,IntStream> localVoisins,
				   Function<BiTree,IntStream> colorLeaf,
				   Predicate<BiTree> isolated){
		if( parent == null || isolated.test(this) )
		    return IntStream.empty();

		return IntStream.concat(
					localVoisins.apply(parent,place)
					.mapToObj( parent.childs::get )
					.flatMapToInt( colorLeaf ),
					parent.colorVoisins(localVoisins,colorLeaf,isolated)
					);
	    }
				   

	    IntStream whiteVoisins(IntPredicate canBeWhite){
		return colorVoisins( (g,v) -> g.whiteVoisinsLocal(v),
				     n -> n.canBeWhiteLeaf(canBeWhite),
				     n -> n.blackIsolation );
	    }			   

	    IntStream blackVoisins(IntPredicate canBeBlack){
		return colorVoisins( (g,v) -> g.blackVoisinsLocal(v),
				     n -> n.canBeBlackLeaf(canBeBlack),
				     n -> n.whiteIsolation );
	    }
	}

	Color[] colors;
	BiTree[] positions;

	ModuleTree(Tree<TriModule.Data,TriModule.IntegerColor> input){
	    int size = 0;
	    for(Tree<TriModule.Data,TriModule.IntegerColor> t : input)
		if( t.isSingleton() )
		    size++;

	    colors = new Color[size];
	    positions = new BiTree[size];

	    bitreeInitiation(null, input);
	}

	ModuleTree(TriGraph g){
	    this( TriModule.decomposition(g).leafMap( x -> new TriModule.IntegerColor(x,g.colorOf(x)) ) );
	}

	private void bitreeInitiation(BiTree parent, Tree<TriModule.Data,TriModule.IntegerColor> cur){
	    if( cur.isSingleton() ){
		TriModule.IntegerColor v = cur.getLeaf();
		colors[v.num] = v.color;
		positions[v.num] = new BiTree(v.num,parent);
	    }else{
		BiTree res = new BiTree(cur.getNode(), parent);
		
		for(Tree<TriModule.Data,TriModule.IntegerColor> c : cur.childs())
		    bitreeInitiation(res,c);

		if( parent == null )
		    res.setIsolation();
	    }
	}

	public Tree<TriModule.Data,Integer> getTrimodularDecomposition(){
	    BiTree root = positions[0].getFirst();
	    return getTrimodularDecomposition(root);
	}

	private Tree<TriModule.Data,Integer> getTrimodularDecomposition(BiTree cur){
	    if( cur.isLeaf() )
		return new Tree<>(cur.leaf);
	    else{
		List<Tree<TriModule.Data,Integer>> childs = cur.childs.stream()
		    .map( this::getTrimodularDecomposition )
		    .collect( Collectors.toCollection(ArrayList::new) );
		
		return Tree.withChilds(cur.node,childs);
	    }
	}

	public IntStream vertices(){
	    return IntStream.range(0,size());
	}

	public IntPaire bicolorRange(){
	    IntSummaryStatistics tmp = IntStream.range(0,colors.length)
		.filter( x -> colors[x] == Color.BICOLOR )
		.summaryStatistics();
	    return new IntPaire(tmp.getMin(),tmp.getMax());
	}

	public IntPaire blacksRange(){
	    IntSummaryStatistics tmp = IntStream.range(0,colors.length)
		.filter( x -> colors[x] == Color.BLACK )
		.summaryStatistics();
	    return new IntPaire(tmp.getMin(),tmp.getMax());
	}

	public IntPaire whitesRange(){
	    IntSummaryStatistics tmp = IntStream.range(0,colors.length)
		.filter( x -> colors[x] == Color.WHITE )
		.summaryStatistics();
	    return new IntPaire(tmp.getMin(),tmp.getMax());
	}

	public int getAsBlack(IntSet x){
	    return x.stream()
		.filter( i -> colors[i] != Color.WHITE )
		.findAny().orElseThrow();
	}

	public int getAsWhite(IntSet x){
	    return x.stream()
		.filter( i -> colors[i] != Color.BLACK )
		.findAny().orElseThrow();
	}

	public Color colorOf(int x){
	    return colors[x];
	}

	public IntSet blackVoisinsOf(int x){
	    if( isBlack(x) )
		return IntSet.EMPTY;
	    
	    return new IntSet( positions[x].blackVoisins(this::canBeBlack) );
	}

	public IntSet whiteVoisinsOf(int x){
	    if( isWhite(x) )
		return IntSet.EMPTY;
	    
	    return new IntSet( positions[x].whiteVoisins(this::canBeWhite) );
	}

	public long arcSize(){
	    return vertices().mapToLong(this::whiteDegreeOf).sum();
	}

	public int bicolorSize(){
	    return (int) vertices()
		.filter(this::isBicolor)
		.count();
	}

	public int blackSize(){
	    return (int) vertices()
		.filter(this::isBlack)
		.count();
	}

	public int whiteSize(){
	    return (int) vertices()
		.filter(this::isWhite)
		.count();
	}

	public int size(){
	    return colors.length;
	}

	public IntStream blackVertices(){
	    return vertices().filter(this::isBlack);
	}

	public IntStream bicolorVertices(){
	    return vertices().filter(this::isBicolor);
	}

	public IntStream whiteVertices(){
	    return vertices().filter(this::isWhite);
	}
	}
}
