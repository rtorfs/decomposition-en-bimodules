import java.util.List;
import java.util.ArrayList;

import java.nio.file.Files;
import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import java.util.function.Function;
public class Reader{

    /** lis les arc dans le fichier name en supposant que les sommets sont des entiers */
    public static List<Paire<Integer>> integerArcs(String name){
	return arcs(name,Integer::valueOf);
    }

    /** lis les arc dans le fichier name en supposant que les sommets sont des chaines de character */
    public static List<Paire<String>> stringArcs(String name){
	return arcs(name, s -> s );
    }

    /** lis dans le fichier name les arcs en parsant les sommets avec reader */
    public static <T> List<Paire<T>> arcs(String name, Function<String,T> reader){
	List<Character> commentSign = List.of('%','#');
	try (Stream<String> f = Files.lines(new File(name).toPath()) ){
	    return f.filter( s -> !s.isEmpty() && !commentSign.contains(s.charAt(0)) )
		.map( l -> l.split("\\s") )
		.filter( l -> l.length >= 2 )
		.map( a -> new Paire<>(reader.apply(a[0]),reader.apply(a[1])) )
		.collect(Collectors.toList());
	}catch(IOException e){
	    System.err.println("erreur :(\n" + e);
	    return null;
	}
    }
    
}
