import java.util.function.Predicate;
import java.util.function.IntPredicate;
import java.util.function.BiPredicate;
import java.util.function.IntFunction;
import java.util.function.Function;

import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

import java.util.List;
import java.util.ArrayList;

import java.util.Set;
import java.util.HashSet;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;
import java.util.Arrays;

import java.util.Vector;

import java.util.HashMap;
public class BiGraph extends Graph{

    int wbLimit;

    /** les blancs sont à gauche et les noirs à droite */
    public static <T,U> InterfaceGraph<U,BiGraph> ofPaires(Iterable<Paire<T>> l, Function<T,U> ordab, Function<T,U> ordaw){
	HashMap<T,Integer> whites = new HashMap<>();
	HashMap<T,Integer> blacks = new HashMap<>();

	Vector<U> whiteName = new Vector<>();
	Vector<U> blackName = new Vector<>();
	
	int numWhite = 0;
	int numBlack = 0;
	
	for(Paire<T> p : l){
	    if( !whites.containsKey(p.fst) ){
		whites.put(p.fst,numWhite);
		whiteName.add( ordaw.apply(p.fst) );
		numWhite++;
	    }
	    if( !blacks.containsKey(p.snd) ){
		blacks.put(p.snd,numBlack);
		blackName.add( ordab.apply(p.snd) );
		numBlack++;
	    }   
	}
	
	whiteName.addAll(blackName);
	ModGraph res = new ModGraph();
	for(Paire<T> p : l)
	    res.add(whites.get(p.fst), numWhite + blacks.get(p.snd), false);
	final int nbLimit = numWhite;
	return new InterfaceGraph<>( res.toGraph( f -> new BiGraph(f,nbLimit) ) , whiteName);
    }
    public static <T> InterfaceGraph<T,BiGraph> ofPaires(Iterable<Paire<T>> l){
	return ofPaires(l, x->x, x->x );
    }

    public InterfaceGraph<Integer,BiGraph> noJumeau(){
	Comparator<Integer> comp = Comparator.comparingInt​(this::degree).thenComparing​(this::voisins);
	Integer[] poso = IntStream.range(0,size()).boxed()
	    .sorted( comp )
	    .toArray(Integer[]::new);

	HashSet<Integer> wrong = new HashSet<>();
	for(int i = 1; i < size(); i++){
	    if( voisins( poso[i] ).equals(voisins( poso[i-1] )) )
		wrong.add( poso[i] );
	    //System.out.println(voisins(i));
	    //System.out.println( IntSet.compare(voisins(i),voisins(i-1)) );
	}
	
	return biFilter( x -> !wrong.contains(x) );
    }

    public IntStream whites(){
	return IntStream.range(0,wbLimit);
    }

    public IntStream blacks(){
	return IntStream.range(wbLimit,size());
    }

    public InterfaceGraph<Integer,BiGraph> noUnIsolated(){
	Set<Integer> wrong = sinks();
	if( !wrong.isEmpty() )
	    return biFilter( x -> !wrong.contains(x) ).apply( BiGraph::noUnIsolated );
	
	int numBlack = blackSize();
	Set<Integer> wWrong = whites().filter( w -> degree(w) == numBlack )
	    .boxed().collect( Collectors.toUnmodifiableSet() );
	Set<Integer> bWrong = blacks().filter( b -> degree(b) == wbLimit )
	    .boxed().collect( Collectors.toUnmodifiableSet() );
	
	if( !bWrong.isEmpty() || !wWrong.isEmpty() )
	    return biFilter( x -> isWhite(x) ? !wWrong.contains(x) : !bWrong.contains(x) ).apply( BiGraph::noUnIsolated );

	return InterfaceGraph.identity(this);
    }

    BiGraph(Stream<IntSet> s, int numWhite){
	super(s);
	wbLimit = numWhite;
    }

    /** graphe biparti avec comme arrêtes l, les points < numWhite sont blancs */
    BiGraph(int size, IntFunction<IntSet> calcVoisins, int numWhite){
	super(size,calcVoisins);
	this.wbLimit = numWhite;
    }
    
    /** graphe bipartis sur les sommets vert (x,y)∈E ssi isArc.test(x,y) */
    BiGraph(int size, BiPredicate<Integer,Integer> isArc, int numWhite){
	super(size, x -> {
		int start = x < numWhite ? numWhite : 0;
		int end = x < numWhite ? size : numWhite;

		return new IntSet( IntStream.range(start,end).filter( y -> isArc.test(x,y) ) );
	    });
	wbLimit = numWhite;
    }

    /** prend un graphe bipartis en entré, avec [0;numWhite[ blanc, et en fait une copie */
    BiGraph(Graph g, int numWhite){
	super(g);
	wbLimit = numWhite;
    }

    public int whiteSize(){
	return wbLimit;
    }

    public int blackSize(){
	return arcs.length - wbLimit;
    }

    /** renvoi si x est blanc */
    public boolean isWhite(int x){
	return x < wbLimit;
    }

    /** renvoi si x et y ont la même couleur */
    public boolean sameColor(int x, int y){
	return isWhite(x) == isWhite(y);
    }

    /** renvoi le bicomplement de this */
    public BiGraph biComplement(){
	return new BiGraph(size(), (x,y) -> !this.isVoisin(x,y), wbLimit);
    }

    /** enleve les sommets qui ne respectent pas isSommet et les arc qui ne respectent pas isArc */
    public InterfaceGraph<Integer,BiGraph> biFilter(IntPredicate isSommet, BiPredicate<Integer,Integer> isArc){
	int numWhite = (int)
	    whites()
	    .filter(isSommet)
	    .count();
	
	return this.filter(isSommet,isArc).apply( g -> InterfaceGraph.identity(new BiGraph(g,numWhite)) );
    }
    public InterfaceGraph<Integer,BiGraph> biFilter(IntPredicate isSommet){
	return biFilter(isSommet,(x,y) -> true);
    }

    /** l'ensemble des sommets blancs */
    /*public Iterable<T> whites(){
	return vertices(this::isWhite);
	}*/

    /** l'ensemble des sommets noirs */
    /*public Iterable<T> blacks(){
	return vertices( (x) -> !isWhite(x) );
	}*/

    /** le graphe d'inclusion uniquement sur les blancs */
    public InterfaceGraph<Integer,Graph> whiteVoisinInclusionGraph(){
	return this.voisinInclusionGraph().filter(this::isWhite);
    }

    /** le graphe d'inclusion uniquement sur les noirs */
    public InterfaceGraph<Integer,Graph> blackVoisinInclusionGraph(){
	return this.voisinInclusionGraph().filter( (x) -> !isWhite(x) );
    }

    public Set<Integer> minBimod(Set<Integer> s){
	int repW = -1;
	int repB = -1;
	
	HashSet<Integer> res = new HashSet<>();
	HashSet<Integer> wait = new HashSet<>();

	wait.addAll(s);
	while( !wait.isEmpty() ){
	    int x = wait.iterator().next();
	    wait.remove(x);
	    res.add(x);
		
	    IntSet diff = new IntSet();
	    if( isWhite(x) )
		if( repW < 0 )
		    repW = x;
		else
		    diff = voisins(repW).xor(voisins(x));
	    else
		if( repB < 0 )
		    repB = x;
		else
		    diff = voisins(repB).xor(voisins(x));
		
	    diff.stream()
		.filter( y -> !res.contains(y) )
		.forEach( wait::add );
	}
	return res;
    }
}
