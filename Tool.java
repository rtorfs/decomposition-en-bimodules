import java.util.function.Predicate;
import java.util.function.IntPredicate;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import java.util.Collection;

import java.util.Set;
import java.util.HashSet;

import java.util.List;
import java.util.ArrayList;
import java.util.Vector;

import java.util.Iterator;
import java.util.Arrays;

import java.util.HashMap;

import java.util.stream.Stream;
import java.util.stream.IntStream;

import java.io.DataOutput;
import java.io.DataInput;
import java.io.IOException;

public class Tool{

    /** renvoi un Iterable de l sans les élément ne respectant pas choose */
    static <T> Iterable<T> chooseIteration(Iterable<T> l, Predicate<T> choose){
	return new Iterable<>(){

	    public Iterator<T> iterator(){
		final Iterator<T> base = l.iterator();
		return new Iterator<>(){

		    T current;
		    boolean empty = false;
		    boolean upToDate = false;
		    
		    private void goNext(){
			if( upToDate )
			    return;
			else
			    upToDate = true;
			
			while( base.hasNext() ){
			    current = base.next();
			    if( choose.test(current) ){
				return;
			    }
			}
			empty = true;
		    }
		    
		    public boolean hasNext(){
			goNext();
			return !empty;
		    }
		    
		    public T next(){
			goNext();
			upToDate = false;
			return current;
		    }
		    
		};
	    }
	    
	};
    }

    static int[] collectionToIntArray(Collection<Integer> l){
	int[] res = new int[ l.size() ];
	int i = 0;
	for(Integer x : l){
	    res[i] = x;
	    i++;
	}
	return res;
    }
    
    /** 
     * affiche un ensemble d'enseemble de paire 
     * si simpl est vrai alors n'affiche que les sommets touché par une paire et pas les paires
     */
    static void printSetSetPaire(Set<Set<IntPaire>> s, boolean simpl){
	System.out.println("{");
	for(Set<IntPaire> x : s)
	    if(x.size() > 1)
		System.out.println("\t" + (simpl ? verticesOfEdges(x) : x));
	System.out.println("}");
    }

    /** renvoi les sommets touché par une paire de s */
    static IntSet verticesOfEdges(Iterable<IntPaire> s){
	Set<Integer> res = new HashSet<>();
	for(IntPaire p : s){
	    res.add(p.fst);
	    res.add(p.snd);
	}
	return new IntSet(res);
    }

    @SafeVarargs
    static <T> List<T> listOfIter(T... t){
	List<T> res = new ArrayList<>();
	for(T s : t)
	    res.add(s);
	return res;
    }

    static <T,U> Set<U> setMap(Set<T> s, Function<T,U> f){
	Set<U> res = new HashSet<>();
	for(T x : s)
	    res.add( f.apply(x) );
	return res;
    }

    static <T> Set<T> setFilter(Set<T> s, Predicate<T> p){
	Set<T> res = new HashSet<>();
	for(T x : s)
	    if( p.test(x) )
		res.add( x );
	return res;
    }

    static <T,U> Set<U> setFilterMap(Set<T> s,Predicate<T> p, Function<T,U> f){
	return setMap(setFilter(s,p),f);
    }

    /** renvoi une String de même longueur que s mais composé uniquement d'espace */
    static String wordToSpace(String s){
	StringBuilder res = new StringBuilder();
	for(int i = 0; i < s.length(); i++)
	    res.append(' ');
	return res.toString();
    }
    
    /** renvoi le produit cartesien de a et b */
    static <T> Stream<Paire<T>> cartesianProduct(Stream<T> a, Supplier<Stream<T>> b){
	return a.flatMap( x ->
			  b.get().map( y -> new Paire<>(x,y) )
			  );
    }
    static Stream<IntPaire> cartesianProduct(IntStream a, Supplier<IntStream> b){
	return a.boxed().flatMap( x ->
				  b.get().mapToObj( y -> new IntPaire(x,y) )
				  );
    }

    /** l'ensemble de s ayant le plus grand cardinal */
    static <T> Set<T> max(Set<Set<T>> s){
	Set<T> res = null;
	for(Set<T> x : s){
	    if( res == null || x.size() > res.size() )
		res = x;
	}
	return res;
    }

    static <T,U> Vector<T> vectorMap(Vector<U> v, Function<U,T> map){
	Vector<T> res = new Vector<>(v.size());
	for(U x : v)
	    res.add( map.apply(x) );
	return res;
    }

    static class BoolArray{

	final byte[] data;
	
	void set(int pos, boolean value){
	    int tmp;
	    if( value )
		tmp = data[pos/8] | (1<<(pos%8));
	    else
		tmp = data[pos/8] & ~(1<<(pos%8));
	    data[pos/8] = (byte) tmp;
	}

	boolean get(int pos){
	    int tmp = data[pos/8] & (1<<(pos%8));
	    return tmp != 0;
	}

	BoolArray(int len){
	    data = new byte[ ((len-1)/8) + 1 ];
	}

	BoolArray(byte[] dt){
	    data = dt;
	}

	public String toString(){
	    String res = "";
	    for(byte b : data)
		res += binary​(b) + "|";
	    return res;
	}
    }

    static String binary(byte b){
	String res = "";
	for(int i = 0; i < 8; i++)
	    res += ((b>>i) & 1) == 1 ? "1": "0";
	return res;
    }

    static class LongStack{

	long[] data;
	private int len;

	LongStack(){
	    len = 0;
	    data = new long[10];
	}

	void push(long a){
	    if( len == data.length ){
		int nlen = (int) (len * 1.1);
		data = Arrays.copyOf(data, nlen);
	    }
	    data[len] = a;
	    len++;
	}

	int size(){
	    return len;
	}

	long pop(){
	    long res = data[len-1];
	    len--;
	    if( data.length > 1000 && (data.length * 0.5) > len ){
		int nlen = (int) (data.length * 0.75);
		data = Arrays.copyOf(data, nlen);
	    }
	    return res;
	}

	boolean isEmpty(){
	    return len == 0;
	}

	
	private static final long LIMIT = 1L << Integer.SIZE;
	IntPaire popPaire(){
	    final long res = pop();
	    
	    final long fst = res>>Integer.SIZE;
	    final long snd = res%LIMIT;
	    
	    return new IntPaire((int) fst,(int) snd);
	}
	
	void push(IntPaire p){
	    push( (p.fst*LIMIT) + (p.snd) );
	}
    }

    static class LazyBiMap<T,U>{
	HashMap<Paire<T>,U> map;
	BiFunction<T,T,U> calc;

	LazyBiMap(BiFunction<T,T,U> calc){
	    this.calc = calc;
	    map = new HashMap<>();
	}

	public U get(T x, T y){
	    Paire<T> tmp = new Paire<>(x,y);
	    if( map.containsKey(tmp) ){
		return map.get(tmp);
	    }else{
		U res = calc.apply(x,y);
		map.put(tmp,res);
		return res;
	    }
	}

	public void set(T x, T y, U z){
	    map.put(new Paire<>(x,y),z);
	}
    }
    
    static Predicate<IntPaire> predicateBoth(IntPredicate ip){
	return p -> ip.test(p.fst) && ip.test(p.snd);
    }


    /*static class DataIO implements DataInput, DataOutput{

	List<Byte> dt = new ArrayList<>();
	int index = 0;
	
	public void write​(byte[] b){
	    for(byte bb : b)
		writeByte(bb);
	}
	
	public void write​(byte[] b, int off, int len){
	    throw new UnsupportedOperationException();
	}
	    
	public void write​(int b){
	    throw new UnsupportedOperationException();
	}
	
	public void writeBoolean​(boolean v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeByte​(int v){
	    dt.add((byte) v);
	}
	
	public void writeBytes​(String s){
	    throw new UnsupportedOperationException();
	}
	
	public void writeChar​(int v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeChars​(String s){
	    throw new UnsupportedOperationException();
	}
	
	public void writeDouble​(double v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeFloat​(float v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeInt​(int v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeLong​(long v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeShort​(int v){
	    throw new UnsupportedOperationException();
	}
	
	public void writeUTF​(String s){
	    throw new UnsupportedOperationException();
	}

	public void readFully(byte[] b){
	    throw new UnsupportedOperationException();
	}
	
	public void readFully​(byte[] b, int off, int len){
	    throw new UnsupportedOperationException();
	}
	
	public String readLine(){
	    throw new UnsupportedOperationException();
	}
       
	public boolean readBoolean​(){
	    throw new UnsupportedOperationException();
	}
	
	public byte readByte​(){
	    byte res = dt.get(index);
	    index++;
	    return res;
	}
	
	public String readBytes​(){
	    throw new UnsupportedOperationException();
	}
	
	public char readChar​(){
	    throw new UnsupportedOperationException();
	}
	
	public String readChars​(){
	    throw new UnsupportedOperationException();
	}
	
	public double readDouble​(){
	    throw new UnsupportedOperationException();
	}
	
	public float readFloat​(){
	    throw new UnsupportedOperationException();
	}
	
	public int readInt​(){
	    throw new UnsupportedOperationException();
	}
	
	public long readLong​(){
	    throw new UnsupportedOperationException();
	}
	
	public short readShort​(){
	    throw new UnsupportedOperationException();
	}
	
	public int readUnsignedShort​(){
	    throw new UnsupportedOperationException();
	}
	
	public int readUnsignedByte(){
	    throw new UnsupportedOperationException();
	}

	public int skipBytes(int x){
	    throw new UnsupportedOperationException();
	}
	
	public String readUTF​(){
	    throw new UnsupportedOperationException();
	}
    }

    static void test(long v, int l) throws IOException{
	DataIO tmp = new DataIO();
	writePositiveNumber(tmp,v,l);
        long r = readPositiveNumber(tmp,l);
	if( r != v )
	    System.out.println(">"+r+"!="+v);
	
	
    }

    static void test() throws IOException{
	//test( 0x100 , 8);
	for(int i = 0; i < 1 + (1<<31); i++)
	  test(i,4);
	  }*/

    /*private static final byte[] BYTE_NUMBER = {
	0b00_00_00_00,
	~0b01_11_11_11,
	~0b00_11_11_11,
	~0b00_01_11_11,
	~0b00_00_11_11,
	~0b00_00_01_11,
	~0b00_00_00_00,
	~0b00_00_00_01
	};*/
    static void writePositiveNumber(DataOutput dt, final long l, final int numSize) throws IOException{
	int dataSize = l == 0 ? 1 : 64 - Long.numberOfLeadingZeros​(l) + 1;
	int usefullBytes = ((dataSize-1) / 8)+1;
	
	BoolArray ba = new BoolArray(dataSize + (numSize - usefullBytes));

	for(int i = 0; i < dataSize; i++)
	    ba.set((ba.data.length * 8) - 1 - i, ((1L << i) & l) != 0 );
	    
	for(int i = 0; i < numSize - ba.data.length; i++)
	    ba.set(i,true);

	dt.write(ba.data);
    }

    private static int leadingOne(byte b){
	BoolArray ba = new BoolArray(new byte[]{ b });
	for(int i = 0; i < 8; i++)
	    if( !ba.get(i) )
		return i;
	throw new IllegalStateException();
    }
    
    static long readPositiveNumber(DataInput dt, final int numSize) throws IOException{
	byte first = dt.readByte();
	int octnum = numSize - leadingOne(first);
	
	byte[] res = new byte[octnum];
	res[0] = first;
	
	for(int i = 1; i < res.length; i++)
	    res[i] = dt.readByte();

	BoolArray ba = new BoolArray(res);

	long l = 0;
	for(int i = leadingOne(first) + 1; i < ba.data.length * 8; i++)
	    if( ba.get(i) )
		l = (l * 2) + 1;
	    else
		l = l * 2;
	    
	return l;
    }

    static long readPositiveLong(DataInput dt) throws IOException{
	return readPositiveNumber(dt,8);
    }

    static int readPositiveInt(DataInput dt) throws IOException{
	return (int) readPositiveNumber(dt,4);
    }

    static void writePositiveInt(DataOutput dt, int l) throws IOException{
	writePositiveNumber(dt,l,4);
    }

    static void writePositiveLong(DataOutput dt, final long l) throws IOException{
	writePositiveNumber(dt,l,8);
    }
}
