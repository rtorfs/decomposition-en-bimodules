import java.util.Vector;

import java.util.Set;
import java.util.HashSet;

import java.util.Collection;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
public class ModGraph{

    Vector<Set<Integer>> arcs;

    /** un graphe modifiable aprés sa creation */
    ModGraph(){
	arcs = new Vector<>();
    }

    ModGraph(int size){
	this();
	if( size > 0 )
	    add(size-1);
    }

    public void reorder(IntUnaryOperator f){
	Vector<Set<Integer>> res = IntStream.range(0,arcs.size()).boxed()
	    .sorted( (i,j) -> Integer.compare(f.applyAsInt(i),f.applyAsInt(j)) )
	    .map(arcs::get)
	    .map( s ->
		  s.stream()
		  .map( x -> f.applyAsInt(x) )
		  .collect( Collectors.toCollection(HashSet::new) )
		  )
	    .collect( Collectors.toCollection(Vector::new) );
	arcs = res;
    }

    /** renvoi le nombre de sommets du graph */
    public int size(){
	return arcs.size();
    }

    /** renvoi le degré de x */
    public int degree(int x){
	return arcs.get(x).size();
    }

    /** renvoi les voisins de x */
    public Collection<Integer> voisins(int x){
	return arcs.get(x);
    }

    /** ajoute un sommet x au graphe */
    public void add(int x){
	for(int i = size(); i <= x; i++)
	    arcs.add(new HashSet<>());
    }

    /** ajoute un arc (x,y) au graphe */
    public void add(int x, int y){
	add(x,y,true);
    }

    /** ajoute un arc (y,x) si oriented est faux et un arc (x,y) au graphe */
    public void add(int x, int y, boolean oriented){
	add(x); add(y);
	arcs.get(x).add(y);
	if( !oriented )
	    arcs.get(y).add(x);
    }

    /** ajoute un arc (x,y) au graphe */
    public void add(IntPaire p){
	add(p,true);
    }
    public void add(Paire<Integer> p){
	add(p,true);
    }
    
    /** ajoute un arc p.rev() si oriented est faux et un arc p au graphe */
    public void add(Paire<Integer> p, boolean oriented){
	add(p.fst,p.snd,oriented);
    }
    public void add(IntPaire p, boolean oriented){
	add(p.fst,p.snd,oriented);
    }

    /** 
     * ajoute un graphe g au graphe 
     * relie (x,y) avec x€this et y€g ssi isArc.test( (x,y) , true )
     * relie (y,x) avec x€this et y€g ssi isArc.test( (x,y) , false )
     */
    public void add(Graph g, BiPredicate<IntPaire,Boolean> isArc){
	int len = size(); 
	for(int x = 0; x < g.size(); x++){
	    add(len + x);
	    for(int y : g.voisins(x))
		add(len+x,len+y);
	}
	
	for(int i = 0; i < len; i++)
	    for(int j = len; j < size(); j++){
		if( isArc.test(new IntPaire(i,j - len),true) )
		    add(i,j);
		if( isArc.test(new IntPaire(i,j - len),false) )
		    add(j,i);
	    }
    }
    public void add(Graph g){
	add(g, (x,y) -> false);
    }
    public void add(ModGraph g, BiPredicate<IntPaire,Boolean> isArc){
	int len = size(); 
	for(int x = 0; x < g.size(); x++){
	    add(len + x);
	    for(int y : g.voisins(x))
		add(len+x,len+y);
	}
	
	for(int i = 0; i < len; i++)
	    for(int j = len; j < size(); j++){
		if( isArc.test(new IntPaire(i,j - len),true) )
		    add(i,j);
		if( isArc.test(new IntPaire(i,j - len),false) )
		    add(j,i);
	    }
    }
    public void add(ModGraph g){
	add(g, (x,y) -> false);
    }

    public static ModGraph cycle(int size){
	ModGraph res = new ModGraph(size);
	if( size > 1 )
	    for(int i = 0; i < size; i++)
		res.add(i, (i+1) % size);
	return res;
    }

    public static ModGraph path(int size){
	ModGraph res = new ModGraph(size);
	for(int i = 0; i < size-1; i++)
	    res.add(i, i+1);
	return res;
    }

    public Graph toGraph(){
	return toGraph(Graph::new);
    }

    public <U> U toGraph(Function<Stream<IntSet>,U> constructor){
	return constructor.apply( IntStream.range(0,size()).mapToObj( this::voisins ).map( IntSet::new ) );
    }

    public String toString(){
	String res = "ModGraph\n";
	int i = 0;
	for(Set<Integer> v : arcs){
	    res += ""+i+":\t";
	    for(int x : v)
		res = res + x + "\t";
	    res = res + "\n";
	    i++;
	}
	return res;
    }
}
