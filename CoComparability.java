import java.util.Set;
import java.util.HashSet;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Vector;

import java.util.Stack;

import java.util.Arrays;
import java.util.Objects;

import java.util.Optional;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import java.util.function.IntPredicate;
import java.util.function.IntFunction;
import java.util.function.Predicate;
public class CoComparability{

    /** renvoi la classe d'implication de g contenant e.fst et e.snd */
    public static Set<IntPaire> implicationClass(BiGraph g, IntPaire e){
	Set<IntPaire> res = new HashSet<>();
	Stack<IntPaire> wait = new Stack<>();
	wait.push(e);

	while( !wait.empty() ){

	    final IntPaire cur = wait.pop();
	    final int b1 = cur.fst;
	    final int b2 = cur.snd;
	    
	    if( !res.add( cur ) ) //on traite deja la paire cur
		continue;

	    for(int w1 : g.voisins(b1) )
		for(int w2 : g.voisins(b2))
		    if( w1 != w2
			&& isIndependantEdges(g,b1,w1,b2,w2)){
			IntPaire tmp = new IntPaire(w1,w2);
			if( !res.contains(tmp) )
			    wait.push( new IntPaire(w1,w2) );
		    }
	}
	return res;
    }


    /** renvoi la classe d'implication de g contenant x et y */    
    public static Set<IntPaire> implicationClass(BiGraph g, int x, int y){
	return implicationClass(g, new IntPaire(x,y));
    }

    /** renvoi toutes les classes d'implication sans orientation 
     * si (x,y) est calculé alors on ignore (y,x) 
     */
    public static Set<Set<IntPaire>> allImplicationClass(BiGraph g){
	return allImplicationClass(g,false);
    }

    /** renvoi toutes les classes d'implication
     * si cpy est faux alors
     * si (x,y) est calculé alors on ignore (y,x) 
     */
    public static Set<Set<IntPaire>> allImplicationClass(BiGraph g, boolean cpy){
	Set<Set<IntPaire>> res = new HashSet<>();
	Set<IntPaire> seen = new HashSet<>();
	for(int b1 = 0; b1 < g.wbLimit; b1++)
	    for(int b2 = 0; b2 < g.wbLimit; b2++)
		if( b1 != b2 )
		    allImplicationClassAux(g, res, seen, new IntPaire(b1,b2));
		else if( !cpy )
		    break;
	for(int w1 = g.wbLimit; w1 < g.size(); w1++)
	    for(int w2 = g.wbLimit; w2 < g.size(); w2++)
		if( w1 != w2 )
		    allImplicationClassAux(g, res, seen, new IntPaire(w1,w2));
		else if( !cpy )
		    break;
	return res;
    }

    private static class SeenArray{

	int minVertice;
	
	Tool.BoolArray[] seenArray;

	SeenArray(IntPaire range){
	    minVertice = range.fst;
	    seenArray =
		IntStream.range(range.fst,range.snd)
		.map( x -> x - minVertice)
		.mapToObj(Tool.BoolArray::new)
		.toArray(Tool.BoolArray[]::new);
	}

	/** b > a*/
	boolean seen(int a, int b){
	    return seenArray[b - minVertice].get(a - minVertice);
	}
	boolean seen(IntPaire p){
	    return seen(p.fst,p.snd);
	}
	
	void see(int a, int b){
	    seenArray[b - minVertice].set(a - minVertice,true);
	}
	void see(IntPaire p){
	    see(p.fst,p.snd);
	}
	
	void unSee(int a, int b){
	    seenArray[b - minVertice].set(a - minVertice,false);
	}
	void unSee(IntPaire p){
	    unSee(p.fst,p.snd);
	}

	@Override
	public String toString(){
	    String res = "";
	    for(Tool.BoolArray a : seenArray)
		res = res + a + "\n";
	    return res;
	}
    }

    private static class BiSeenArray{
	SeenArray whites;
	SeenArray blacks;
	IntPredicate isWhite;

	BiSeenArray(IntPaire wrange, IntPaire brange, IntPredicate isWhite){
	    this.isWhite = isWhite;
	    whites = new SeenArray(wrange);
	    blacks = new SeenArray(brange);
	}

	public boolean seen(IntPaire p){
	    if( isWhite.test(p.fst) && isWhite.test(p.snd) )
		return whites.seen(p);
	    if( !isWhite.test(p.fst) && !isWhite.test(p.snd) )
		return blacks.seen(p);
	    throw new IllegalStateException();
	}

	public void see(IntPaire p){
	    if( isWhite.test(p.fst) && isWhite.test(p.snd) ){
		whites.see(p);
		return;
	    }
	    if( !isWhite.test(p.fst) && !isWhite.test(p.snd) ){
		blacks.see(p);
		return;
	    }
	    throw new IllegalStateException();
	}
    }

    public static Set<IntSet> allNonTrivialImplicationClass(BiGraph g){
	Set<IntSet> res = new HashSet<>();
	BiSeenArray sa = new BiSeenArray(new IntPaire(0,g.wbLimit),
				       new IntPaire(g.wbLimit,g.size()),
				       g::isWhite);
	
	for(int i = 0; i < g.whiteSize(); i++)
	    for(int j = i+1; j < g.whiteSize(); j++){
		IntPaire p = new IntPaire(i,j);
		if( !sa.seen(p) )
		    switch( g.voisins(i).inclusionType(g.voisins(j)) ){
		    case EQUAL:
			throw new IllegalStateException("found jumeaux");
		    case OVERLAP:
		    case DISJOINT:
			sa.see(p);
			res.add( allNonTrivialImplicationClassAux(g,p,sa) );
		    }
	    }
	
	    Main.log.messageAge("implication classes calculated");

	return res;
    }

    private static int maxPro;
    private static void computing(BiGraph g,
				  BiSeenArray sa,
				  Tool.LongStack prevPaire,
				  Tool.LongStack prevPaireIterator,
				  IntPaire p,
				  IntPaire cur){

	sa.see(p);
	if( cur.fst >= g.degree(p.fst) ) // noeud epuisé
	    return;
	if( cur.snd >= g.degree(p.snd) ){
	    computing(g,sa,prevPaire,prevPaireIterator,p,new IntPaire(cur.fst+1,0));
	    return;
	}

	if( prevPaire.size() > maxPro )
	    maxPro = prevPaire.size();
	
	prevPaire.push(p);
	prevPaireIterator.push(new IntPaire(cur.fst,cur.snd+1));

	int u = g.voisins(p.fst).get(cur.fst);
	int v = g.voisins(p.snd).get(cur.snd);
	IntPaire uv = u < v ? new IntPaire(u,v) : new IntPaire(v,u);
	if( u != v && !sa.seen(uv) && isIndependantEdges(g,p.fst,u,p.snd,v) ){
	    prevPaire.push(uv);
	    prevPaireIterator.push(new IntPaire(0,0));
	}
	
    }

    private static IntSet allNonTrivialImplicationClassAux(BiGraph g, IntPaire p, BiSeenArray sa){
	maxPro = 0;
	Set<Integer> res = new HashSet<>();
	
	Tool.LongStack prevPaire = new Tool.LongStack();
	Tool.LongStack prevPaireIterator = new Tool.LongStack();

	prevPaire.push(p);
	prevPaireIterator.push(new IntPaire(0,0));
	
	while( !prevPaire.isEmpty() ){
	    IntPaire pp = prevPaire.popPaire();
	    IntPaire cur = prevPaireIterator.popPaire();

	    res.add(pp.fst); res.add(pp.snd);
	    
	    computing(g,sa,prevPaire,prevPaireIterator,pp,cur);
	}
	return new IntSet(res);
    }

    private static class Seen2DArray{
	int minX;
	int minY;

	IntPredicate isX;
	Tool.BoolArray[] seenArray;

	Seen2DArray(IntPaire xrange, IntPaire yrange, IntPredicate isX){
	    this.isX = isX;
	    minX = xrange.fst;
	    minY = yrange.fst;
	    seenArray = IntStream.range(xrange.fst,xrange.snd)
		.map( x -> x - minX )
		.mapToObj( x -> new Tool.BoolArray(yrange.snd - yrange.fst) )
		.toArray(Tool.BoolArray[]::new);
	}

	public boolean seen(IntPaire p){
	    IntPaire res = p;
	    if( isX.test(p.fst) )
		if( isX.test(p.snd) )
		    res = IntPaire.smallFirst(p);
		else
		    res = p;
	    else
		if( isX.test(p.snd) )
		    res = p.rev();
		else
		    throw new IllegalArgumentException();
	    return seenArray[res.fst - minX].get(res.snd - minY);
	}

	public void see(IntPaire p){
	    IntPaire res = p;
	    if( isX.test(p.fst) )
		if( isX.test(p.snd) )
		    res = IntPaire.smallFirst(p);
		else
		    res = p;
	    else
		if( isX.test(p.snd) )
		    res = p.rev();
		else
		    throw new IllegalArgumentException();
	    seenArray[res.fst - minX].set(res.snd - minY,true);
	}

	@Override
	public String toString(){
	    String res = "";
	    for(Tool.BoolArray a : seenArray)
		res = res + a + "\n";
	    return res;
	}
    }
    
    private static class TriSeenArray{
	SeenArray whiteOnly;
	SeenArray blackOnly;

	Seen2DArray bicolor;
	IntFunction<TriGraph.Color> color;

	TriSeenArray(IntPaire wrange, IntPaire grange, IntPaire brange, IntFunction<TriGraph.Color> color){
	    this.color = color;
	    whiteOnly = new SeenArray(wrange);
	    blackOnly = new SeenArray(brange);

	    int min = Stream.of(wrange,grange,brange)
		.mapToInt( p -> p.fst )
		.min().getAsInt();
	    int max = Stream.of(wrange,grange,brange)
		.mapToInt( p -> p.snd )
		.max().getAsInt();
	    bicolor = new Seen2DArray(grange,new IntPaire(min,max), x -> color.apply(x) == TriGraph.Color.BICOLOR );
	}

	public boolean seen(IntPaire p){
	    switch(color.apply(p.fst)){
	    case WHITE:
		switch(color.apply(p.snd)){
		case WHITE: return whiteOnly.seen(p);
		case BICOLOR: return bicolor.seen(p);
		default:
		    throw new IllegalArgumentException();
		}
	    case BLACK:
		switch(color.apply(p.snd)){
		case BLACK: return blackOnly.seen(p);
		case BICOLOR: return bicolor.seen(p);
		default:
		    throw new IllegalArgumentException();
		}
	    default:
		return bicolor.seen(p);
	    }
	}

	public void see(IntPaire p){
	    switch(color.apply(p.fst)){
	    case WHITE:
		switch(color.apply(p.snd)){
		case WHITE: whiteOnly.see(p); break;
		case BICOLOR: bicolor.see(p); break;
		default:
		    throw new IllegalArgumentException();
		}
		break;
	    case BLACK:
		switch(color.apply(p.snd)){
		case BLACK: blackOnly.see(p); break;
		case BICOLOR: bicolor.see(p); break;
		default:
		    throw new IllegalArgumentException();
		}
		break;
	    default:
		bicolor.see(p);
	    }
	}
	
	@Override
	public String toString(){
	    String res = "";
	    res = res + "blacks only\n" + blackOnly;
	    res = res + "white only\n" + whiteOnly;
	    res = res + "bicolor only\n" + bicolor;
	    return res;
	}
    }

    public static Tree<IntSet,Integer> implicationClassTree(BiGraph g){
	return implicationClassTree(new TriGraph.Immutable(g));
    }

    private static class ModuleData{
	int id;
	ModuleData parent;
	IntSet mod;
	Vector<ModuleData> all;
	int[] parentOf;

	public int size(){
	    return mod.size();
	}

	ModuleData(int[] parentOf, Vector<ModuleData> all){
	    this(new IntSet(0,parentOf.length),0,parentOf,all);
	    setParent(this);
	}

	ModuleData(IntSet m, int id, int[] parentOf, Vector<ModuleData> all){
	    this.mod = m;
	    this.id = id;
	    this.parent = null;
	    this.parentOf = parentOf;
	    this.all = all;
	}

	private void setParent(ModuleData md){
	    if( parent == null || parent == md )
		parent = md;
	    else
		throw new IllegalStateException();
	}

	private void parentNormalisation(Set<Integer> seen, ModuleData md){
	    if( md.size() >= this.size() || seen.contains(md.id) )
		return;

	    seen.add(md.id);
	    if( md.parent.size() > this.size() ){
		setParent(md.parent);
		md.parent = this;
	    }
	    else
		parentNormalisation(seen,md.parent);
	}
	
	static void addNewModule(IntSet m, int[] po, Vector<ModuleData> a){
	    if( m.size() == po.length )
		return;
	    
	    Set<Integer> seen = new HashSet<>();
	    ModuleData res = new ModuleData(m,a.size(),po,a);
	    a.add(res);

	    for(int x : m){
		ModuleData md = a.get(po[x]);
		res.parentNormalisation(seen,md);

		if( res.size() < md.size() ){
		    po[x] = res.id;
		    res.setParent(md);
		}
	    }

	    if( res.parent == null ){
		System.out.println(m);
		throw new IllegalStateException();
	    }
	}

	ModuleData(int x, int[] parentOf, Vector<ModuleData> all){
	    this(IntSet.of(x), -1, parentOf, all);
	    setParent(all.get(parentOf[x]));
	}

	static Paire<IntSet> lastModulesOf(int a, int b, int[] parentOf, Vector<ModuleData> all){
	    ModuleData curA = new ModuleData(a, parentOf, all);
	    ModuleData curB = new ModuleData(b, parentOf, all);

	    while(curA.parent.size() != curB.parent.size()){
		if( curA.parent.size() < curB.parent.size() )
		    curA = curA.parent;
		else
		    curB = curB.parent;
	    }

	    return new Paire<>(curA.mod,curB.mod);
	}
    }
    
    public static Tree<IntSet,Integer> implicationClassTree(TriGraph g){
	return implicationClassTree(g, new boolean[1]);
    }
	
    public static Tree<IntSet,Integer> implicationClassTree(TriGraph g, boolean[] foundTop){
	TriSeenArray sa = new TriSeenArray( g.whitesRange(),g.bicolorRange(),g.blacksRange() , g::colorOf );

	/*for(int i = 0; i < g.size(); i++){
	    System.out.println(">"+i+"->"+Graph.mainPrinter.apply(i));
	    }*/

	int[] smallestModuleOf = g.vertices().map( x -> 0 ).toArray();
	Vector<ModuleData> allModules = new Vector<>();
	allModules.add(new ModuleData(smallestModuleOf,allModules));

	implicationClassTreeAux(g,foundTop,sa,
				allModules,
				smallestModuleOf);
	
	List<Tree<IntSet,Integer>> res = allModules.stream()
	    .map( md -> Tree.withChilds(md.mod,(List<Tree<IntSet,Integer>>) null) )
	    .collect(Collectors.toCollection(ArrayList::new));
	
	allModules.stream()
	    .filter( md -> md.id > 0 )
	    .forEachOrdered( md -> {
		    res.get(md.parent.id).addChild(res.get(md.id));
		});

	for(int i = 0; i < smallestModuleOf.length; i++)
	    res.get(smallestModuleOf[i]).addChild(new Tree<>(i));
	
	Main.log.messageAge("implication classes computed");
	return res.get(0);
    }
    
    private static void implicationClassTreeAux(TriGraph g, boolean[] foundTop,
						TriSeenArray sa,
						Vector<ModuleData> modules,
						int[] smallestModuleOf){

	Tool.cartesianProduct( IntStream.concat(g.whiteVertices(),g.bicolorVertices()),
			       () -> IntStream.concat(g.whiteVertices(),g.bicolorVertices()) )
	    .filter(IntPaire::different)
	    .filter( p -> p.fst < p.snd )
	    .forEachOrdered( p -> {
		    if( sa.seen(p) || !g.isRelevant(p))
			return;

		    IntSet moduleFound = implicationClassTreeSearch(g,sa,modules,smallestModuleOf,p);
		    if( moduleFound.size() == g.size() )
			foundTop[0] = true;
		    ModuleData.addNewModule(moduleFound, smallestModuleOf, modules);
		});
    }
    
    private static IntSet implicationClassTreeSearch(TriGraph g,
						     TriSeenArray sa,
						     Vector<ModuleData> modules,
						     int[] smallestModuleOf,
						     IntPaire p){
	Set<Integer> res = new HashSet<>();
	
	Tool.LongStack wait = new Tool.LongStack();
	wait.push(p);
	
	while( !wait.isEmpty() ){
	    IntPaire cur = wait.popPaire();

	    Paire<IntSet> comm = ModuleData.lastModulesOf(cur.fst,cur.snd,smallestModuleOf,modules);
	    if( !res.contains(cur.fst) )
		comm.fst.stream().forEachOrdered(res::add);
	    if( !res.contains(cur.snd) )
		comm.snd.stream().forEachOrdered(res::add);

	    
	    Tool.cartesianProduct(comm.fst.stream(),comm.snd::stream)
		.filter( mulink -> g.isComparable(mulink.fst,mulink.snd) )
		.map(IntPaire::smallFirst)
		.forEach(sa::see);
	    
	    g.forcing(cur)
		.filter( next -> !sa.seen(next) )
		.filter( next -> !comm.fst.contains(next.fst) || !comm.fst.contains(next.snd) )
		.forEachOrdered( next -> {
			sa.see(next);
			wait.push(next);
		    });
	}
	
	return new IntSet(res);
    }
    
    private static Set<IntPaire> allNonTrivialImplicationClassCalcRelevant(BiGraph g){
	Set<IntPaire> relevant = new HashSet<>();
	for(int w1 = 0; w1 < g.wbLimit; w1++)
	    for(int w2 = w1+1; w2 < g.wbLimit; w2++)
		switch( g.voisins(w1).inclusionType(g.voisins(w2)) ){
		case EQUAL:
		    throw new IllegalStateException();
		case OVERLAP:
		case DISJOINT:
		    if( w1 == w2-1 )
			System.out.println(">>"+w1+"-"+w2);
		    relevant.add( new IntPaire(w1,w2) );
		}
	System.err.println("white relevant Paire done");
	for(int b1 = g.wbLimit; b1 < g.size(); b1++)
	    for(int b2 = b1+1; b2 < g.size(); b2++)
		switch( g.voisins(b1).inclusionType(g.voisins(b2)) ){
		case EQUAL:
		    throw new IllegalStateException();
		case OVERLAP:
		case DISJOINT:
		    relevant.add( new IntPaire(b1,b2) );
		}
	System.err.println("black relevant Paire done");
	return relevant;
    }
    
    private static void allImplicationClassAux(BiGraph g, Set<Set<IntPaire>> res, Set<IntPaire> seen, IntPaire e){
	if( seen.contains(e) )
	    return;

	Set<IntPaire> cla = implicationClass(g, e);
	seen.addAll(cla);
	res.add(cla);
    }

    /** 
     * renvoi si b1-w1 est indépendante de b2-w2 
     * ne verifie pas que (b1,w1)∈E et (b2,w2)∈E
     */
    static boolean isIndependantEdges(BiGraph g, int b1, int w1, int b2, int w2){
	boolean disjointEdges = b1 != b2 && b1 != w2 && w1 != b2 && w1 != w2;
	boolean noCommonEdges = !g.isVoisin(w1,b2) && !g.isVoisin(w2,b1) && !g.isVoisin(b1,b2) && !g.isVoisin(w1,w2);
	return disjointEdges && noCommonEdges;
    }

    /** les arrêtes independantes d'une classe d'equivalence s */
    static Set<IntPaire> independantEdges(BiGraph g, Set<IntPaire> s){
	Set<IntPaire> res = new HashSet<>();
	for(IntPaire p1 : s)
	    for(IntPaire p2 : s){
		if( g.isVoisin(p1.fst,p2.fst) && g.isVoisin(p1.snd,p2.snd) && isIndependantEdges(g, p1.fst, p2.fst, p1.snd, p2.snd) ){
		    res.add(new IntPaire(p1.fst,p2.fst));
		    res.add(new IntPaire(p1.snd,p2.snd));
		}
		if( g.isVoisin(p1.fst,p2.snd) && g.isVoisin(p1.snd,p2.fst) && isIndependantEdges(g, p1.fst, p2.snd, p1.snd, p2.fst) ){
		    res.add(new IntPaire(p1.fst,p2.snd));
		    res.add(new IntPaire(p1.snd,p2.fst));
		}   
	    }
	return res;
    }

    /** renvoi si la classe d'implication s est contradictoire ( (x,y)∈s et (y,x)∈s ) */
    static boolean isImpossible(Set<IntPaire> s){
	IntPaire p = s.iterator().next();
	return s.contains(p.rev());
    }

    /** FIN DE CONTENU **/

    
    // s est deja orienté
    /*private static <T> boolean seen(Set<Paire<T>> s, Graph<T> ori){
	Paire<T> p = s.iterator().next();
	return ori.isVoisin(p.fst,p.snd) || ori.isVoisin(p.snd,p.fst);
	}*/

    /*private static <T> List<Paire<T>> orientation(Graph<T> g, Graph<T> ori, Graph<T> revori, Paire<T> p){
	List<Paire<T>> res = new ArrayList<>();
	T x = p.fst;
	T y = p.snd;
	for(T z : ori.voisins(y)){
	    // x->y->z et xz non-orieté
	    if( g.isVoisin(x,z) && !ori.isVoisin(x,z) )
		res.add(new Paire<>(x,z));
	}
	for(T w : revori.voisins(x)){
	    // w->x->y et wx non-orieté
	    if( g.isVoisin(w,x) && !revori.isVoisin(w,x) )
		res.add(new Paire<>(w,y));
	}
	return res;
    }
    
    public static <T> Paire<List<T>> compute(BiGraph<T> g){
	ModGraph<T> res = new ModGraph<T>();
	ModGraph<T> antires = new ModGraph<T>();
	//LinkedList<Set<T>>
	
	Set<Set<Paire<T>>> impl = allImplicationClass(g,false);
	Set<Set<Paire<T>>> seen = new HashSet<>();
	impl.removeIf( (s) -> s.size() == 1 );
	
	for(Set<Paire<T>> c : impl){
	    if( seen(c,res) )
		continue;
	    if( isImpossible(c) )
		return null;
	    for(Paire<T> arc : c){
		res.add(arc);
		antires.add(arc.rev());
	    }
	    seen.add(c);
	}
	return null;
	}*/
}
