import java.util.Arrays;

import java.util.Set;
import java.util.HashSet;

import java.util.Vector;

import java.util.Stack;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
public class StronglyConnectedComponents{

    /**
     * calcul des composantes fortement connexe d'un graphe
     * source:
     *
     * https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm#The_algorithm_in_pseudocode
     */

    private int index = 0;
    private final Stack<Integer> stack = new Stack<>();
    private final Vector<Set<Integer>> results = new Vector<>();
    private final int[] indexs;
    private final int[] lowLink;
    private final boolean[] onStack;
    private final Graph g;

    private StronglyConnectedComponents(Graph g){
	this.g = g;
	indexs = new int[g.size()];
	lowLink = new int[g.size()];
	onStack = new boolean[g.size()];

	Arrays.fill(indexs,-1);
	Arrays.fill(lowLink,Integer.MAX_VALUE);
	Arrays.fill(onStack,false);
    }

    public static Vector<Set<Integer>> compute(Graph g){
	StronglyConnectedComponents scc = new StronglyConnectedComponents(g);
	
	for(int i = 0; i < scc.indexs.length; i++)
	    if( scc.indexs[i] < 0 )
		scc.strongConnect(i);
	
	return scc.results;
    }

    private static int min(int a, int b){
	return a < b ? a : b;
    }

    private void strongConnect(int v){
        // Set the depth index for v to the smallest unused index
        indexs[v] = index;
	lowLink[v] = index;
        index++;
        stack.push(v);
        onStack[v] = true;
      
        // Consider successors of v
        for(int w : g.voisins(v)){
            if (indexs[w] < 0 ){ //is undefined
                // Successor w has not yet been visited; recurse on it
                strongConnect(w);
                lowLink[v] = min(lowLink[v], lowLink[w]);
	    }else if( onStack[w] ){
                // Successor w is in stack S and hence in the current SCC
                // If w is not on stack, then (v, w) is an edge pointing to an SCC already found and must be ignored
                // Note: The next line may look odd - but is correct.
                // It says w.index not w.lowlink; that is deliberate and from the original paper
                lowLink[v] = min(lowLink[v], indexs[w]);
            }
        }
      
        // If v is a root node, pop the stack and generate an SCC
        if( lowLink[v] == indexs[v] ){
	    Set<Integer> res = new HashSet<>();
	    int w;
            do{
                w = stack.pop();
                onStack[w] = false;
		res.add(w);
	    }while(w != v);
            results.add(res);
        }
    }

    private static class Voisins{
	TriGraph g;
	Set<Integer> whites;
	Set<Integer> blacks;
	Set<Integer> bicolor;

	Voisins(TriGraph g, Set<Integer> w, Set<Integer> b, Set<Integer> bw){
	    this.g = g;
	    whites = w;
	    blacks = b;
	    bicolor = bw;
	}

	Voisins(TriGraph g){
	    this.g = g;

	    whites = g.whiteVertices().boxed().collect(Collectors.toCollection(HashSet::new));
	    blacks = g.blackVertices().boxed().collect(Collectors.toCollection(HashSet::new));
	    bicolor = g.bicolorVertices().boxed().collect(Collectors.toCollection(HashSet::new));
	}

	Paire<Voisins> split(int v){
	    
	    Set<Integer> nw = new HashSet<>();
	    Set<Integer> nb = new HashSet<>();
	    Set<Integer> ng = new HashSet<>();
	    
	    g.voisins(v).forEachOrdered( x -> {
		    if( whites.contains(x) ){
			whites.remove(x);
			nw.add(x);
		    }else if( blacks.contains(x) ){
			blacks.remove(x);
			nb.add(x);
		    }else if( bicolor.contains(x) ){
			bicolor.remove(x);
			ng.add(x);
		    }
		});

	    return new Paire<>(new Voisins(g,nw,nb,ng),this);
	}

	Paire<Voisins> splitCompl(int v){
	    
	    Set<Integer> nw = new HashSet<>();
	    Set<Integer> nb = new HashSet<>();
	    Set<Integer> ng = new HashSet<>();
	    
	    g.voisins(v).forEachOrdered( x -> {
		    if( whites.contains(x) ){
			whites.remove(x);
			nw.add(x);
		    }else if( blacks.contains(x) ){
			blacks.remove(x);
			nb.add(x);
		    }else if( bicolor.contains(x) ){
			bicolor.remove(x);
			ng.add(x);
		    }
		});

	    switch( g.colorOf(v) ){
	    case WHITE:
		Voisins nout = new Voisins(g,whites,nb,ng);
		whites = nw;
		return new Paire<>(this,nout);
	    case BLACK:
		Voisins nout2 = new Voisins(g,nw,blacks,ng);
		blacks = nb;
		return new Paire<>(this,nout2);
	    default:
		return new Paire<>(this,new Voisins(g,nw,nb,ng));
	    }
	}

	private static int pop(Set<Integer> s){
	    int res = s.stream().findAny().orElseThrow();
	    s.remove(res);
	    return res;
	}

	int pop(){
	    if( !whites.isEmpty() )
		return pop(whites);
	    if( !blacks.isEmpty() )
		return pop(blacks);
	    return pop(bicolor);
	}

	IntStream stream(){
	    return IntStream.concat(
				    whites.stream().mapToInt(x->x),
				    IntStream.concat( blacks.stream().mapToInt(x->x),
						      bicolor.stream().mapToInt(x->x)
						      ));
	}

	public String toString(){
	    return "whites: "+whites+"\nblacks: "+blacks+"\nbicolor: "+bicolor;
	}
    }

    public static Vector<Set<Integer>> compute(TriGraph g, boolean compl){
	/*System.out.println("--------------------------------------------------");
	System.out.println(g);
	System.out.println();*/
	Vector<Set<Integer>> res = new Vector<>();

	Vector<Integer> inside = new Vector<>();
	Voisins outside = new Voisins(g);
	
	int cur = 0;
	for(int tot = 0; tot < g.size(); tot++){
	    //System.out.println(">>"+outside);
	    if( cur == inside.size() ){
		if( !inside.isEmpty() )
		    res.add( new HashSet<>(inside) );
		inside.clear();
		inside.add( outside.pop() );
		cur = 0;
	    }
	    int x = inside.get(cur);

	    //System.out.println(">"+x);
	    //System.out.println(outside);
	    Paire<Voisins> inout = compl ? outside.splitCompl(x) : outside.split(x);
	    outside = inout.snd;
	    inout.fst.stream().forEachOrdered(inside::add);
	    
	    cur++;
	}
	if( !inside.isEmpty() )
	    res.add( new HashSet<>(inside) );
    
	return res;
    }
}
