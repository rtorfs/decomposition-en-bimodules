import java.util.Vector;
import java.util.List;
import java.util.Set;

import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
public class InterfaceGraph<T,U extends Graph>{

    Vector<T> vertices;
    U g;

    public T vertice(int p){
	return vertices.get(p);
    }

    InterfaceGraph(U g, Vector<T> v){
	vertices = v;
	this.g = g;	
    }

    InterfaceGraph(U g, List<T> v){
	vertices = new Vector<>(v.size());
	for(T x : v)
	    vertices.add(x);
	this.g = g;	
    }

    InterfaceGraph(BiFunction<Integer,BiPredicate<Integer,Integer>,U> constructor, Vector<T> v, BiPredicate<T,T> arc){
	vertices = v;
	this.g = constructor.apply(v.size(), (x,y) -> arc.test(v.get(x),v.get(y)));
    }

    static <T> InterfaceGraph<T,Graph> ofBase(Set<T> s, BiPredicate<T,T> arc){
	Vector<T> name = new Vector<>(s.size());
	for(T x : s)
	    name.add(x);
	return new InterfaceGraph<>( (len,a) -> new Graph(len,a), name, arc);
    }

    public int size(){
	return g.size();
    }
    
    public long arcSize(){
	return g.arcSize();
    }

    static <U extends Graph> InterfaceGraph<Integer,U> identity(U g){
	Vector<Integer> name = new Vector<>(g.size());
	for(int i = 0; i < g.size(); i++)
	    name.add(i);
	return new InterfaceGraph<>(g,name);
    }

    public <W extends Graph> InterfaceGraph<T,W> composeWith(InterfaceGraph<Integer,W> ig){
	return ig.map( vertices::get );
    }

    public <W extends Graph> InterfaceGraph<T,W> apply(Function<U,InterfaceGraph<Integer,W>> f){
	return this.composeWith( f.apply(this.g) );
    }

    /** 
     * ig est calculé à partir de this 
     * converter prend un élément de ig et convertit tous les entiers pertinend en éléments de this de type T
     * dans le cas A=Integer: converter = (i,f) -> f.apply(i)
     */
    public <A,B,W extends Graph> InterfaceGraph<B,W> composeWith(InterfaceGraph<A,W> ig, BiFunction<A,IntFunction<T>,B> converter){
	return ig.map( (x) -> converter.apply(x,vertices::get) );
    }

    public <A,B,W extends Graph> InterfaceGraph<B,W> apply(Function<U,InterfaceGraph<A,W>> f, BiFunction<A,IntFunction<T>,B> converter){
	return this.composeWith( f.apply(this.g) , converter );
    }

    /** calcule un V à partir de g et remplace les entiers par des T si besoin */
    public <V,W> W compute(Function<U,V> calc, BiFunction<V,IntFunction<T>,W> map){
	return map.apply(calc.apply(g),vertices::get);
    }

    public <A> Tree<A,T> computeTree( Function<U,Tree<A,Integer>> calc ){
	return compute( calc, (t,f) -> t.leafMap((x) -> f.apply(x)) );
    }

    public <W extends Graph> InterfaceGraph<Set<T>,W> applySetGraph( Function<U,InterfaceGraph<Set<Integer>,W>> f ){
	return apply(f, (s,geter) -> Tool.setMap(s, (x) -> geter.apply(x) ) );
    }

    public Set<T> computeSet(Function<U,Set<Integer>> calc){
	return compute(calc, (s,f) -> Tool.setMap(s, (x) -> f.apply(x) ));
    }

    public Vector<Set<T>> computeSets(Function<U,Vector<Set<Integer>>> calc){
	return compute(calc, (s,f) -> Tool.vectorMap(s, (x) -> Tool.setMap(x, (y) -> f.apply(y) )));
    }

    public <V> InterfaceGraph<V,U> map(Function<T,V> map){
	Vector<V> nv = new Vector<>( vertices.size() );
	for(T x : vertices)
	    nv.add( map.apply(x) );
	return new InterfaceGraph<>(g,nv);
    }

    @Override
    public String toString(){
	return g.toString( (i) -> vertices.get(i).toString() );
    }
    
}
